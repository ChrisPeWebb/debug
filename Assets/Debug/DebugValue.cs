﻿using System;
using System.Reflection;
using UnityEngine;


namespace Defiant.Debug
{
    public class DebugValue
    {
        public string Name { get; private set; }
        public string Description { get; private set; }

        private string m_saveKey;
        private bool m_savesAndLoads;

        private readonly FieldInfo m_field;
        private readonly PropertyInfo m_property;
        private readonly object m_instance; // If null, values will be applied to static fields


        public DebugValue(string a_name, string a_description, FieldInfo a_field, object a_instance = null)
        {
            Name = a_name;
            Description = a_description;
            m_field = a_field;
            m_property = null;
            m_instance = a_instance;

            m_savesAndLoads = GetAttribute<DebugDontSaveOrLoadAttribute>() == null;

            LoadValue();
        }


        public DebugValue(string a_name, string a_description, PropertyInfo a_property, object a_instance = null)
        {
            Name = a_name;
            Description = a_description;
            m_property = a_property;
            m_field = null;
            m_instance = a_instance;

            m_savesAndLoads = GetAttribute<DebugDontSaveOrLoadAttribute>() == null;

            LoadValue();
        }


        public Type Type
        {
            get
            {
                if (m_field != null)
                {
                    return m_field.FieldType;
                }

                if (m_property != null)
                {
                    return m_property.PropertyType;
                }

                return null;
            }
        }


        public object Value
        {
            get
            {
                if(m_field != null)
                {
                    return m_field.GetValue(m_instance);
                }

                if (m_property != null)
                {
#if NETFX_CORE
                    return m_property.GetValue(m_instance, null);
#else
                    // WORKAROUND: GetValue breaks under AOT
                    return m_property.GetGetMethod().Invoke(m_instance, null);
#endif
                }

                return null;
            }
            set
            {
                value = ConstrainValue(value);
                // TODO (Chris): Check typing?

                if (m_field != null)
                {
                    m_field.SetValue(m_instance, value);
                }

                if (m_property != null)
                {
#if NETFX_CORE
                    m_property.SetValue(m_instance, value, null);
#else
                    m_property.GetSetMethod().Invoke(m_instance, new[]{value});
#endif
                }

                SaveValue();
            }
        }


        public string SaveKey
        {
            get
            {
                if(m_saveKey == null)
                {
                    if (m_field != null)
                    {
                        m_saveKey = m_field.DeclaringType.Name + '.' + m_field.Name;
                    }
                    else if (m_property != null)
                    {
                        m_saveKey = m_property.DeclaringType.Name + '.' + m_property.Name;
                    }
                    else
                    {
                        m_saveKey = "";
                    }
                }
                return m_saveKey;
            }
        }


        public void StepUp()
        {
            if (Type == typeof(int))
            {
                DebugRangeAttribute rangeAttribute = GetAttribute<DebugRangeAttribute>();

                if (rangeAttribute != null)
                {
                    Value = (int)Value + (int)rangeAttribute.Step;
                }
                else
                {
                    Value = (int)Value + 1;
                }
            }

            if (Type == typeof(float))
            {
                DebugRangeAttribute rangeAttribute = GetAttribute<DebugRangeAttribute>();

                if (rangeAttribute != null)
                {
                    Value = (float)Value + (float)rangeAttribute.Step;
                }
                else
                {
                    Value = (float)Value + 1.0f;
                }
            }
        }
        

        public void StepDown()
        {
            if (Type == typeof(int))
            {
                DebugRangeAttribute rangeAttribute = GetAttribute<DebugRangeAttribute>();

                if (rangeAttribute != null)
                {
                    Value = (int)Value - (int)rangeAttribute.Step;
                }
                else
                {
                    Value = (int)Value - 1;
                }
            }

            if (Type == typeof(float))
            {
                DebugRangeAttribute rangeAttribute = GetAttribute<DebugRangeAttribute>();

                if (rangeAttribute != null)
                {
                    Value = (float)Value - (float)rangeAttribute.Step;
                }
                else
                {
                    Value = (float)Value - 1.0f;
                }
            }
        }


        private object ConstrainValue(object a_value)
        {
            if (Type == typeof(int))
            {
                DebugRangeAttribute rangeAttribute = GetAttribute<DebugRangeAttribute>();

                if (rangeAttribute != null)
                {
                    // Ensure value is a full step. This corrects manual input which can be of non stepped values
                    /*if (rangeAttribute.UsesStep)
                    {
                        int steps = (int)((int)a_value / (int)rangeAttribute.Step);
                        a_value = steps * (int)rangeAttribute.Step;
                    }*/

                    a_value = (int)Mathf.Clamp((int)a_value, rangeAttribute.Min, rangeAttribute.Max);
                }
            }

            if (Type == typeof(float))
            {
                if (Mathf.Approximately((float)a_value, 0.0f))
                {
                    a_value = 0.0f;
                }

                DebugRangeAttribute rangeAttribute = GetAttribute<DebugRangeAttribute>();

                if (rangeAttribute != null)
                {
                    // Ensure value is a full step. This corrects manual input which can be of non stepped values
                    /*if (rangeAttribute.UsesStep)
                    {
                        int steps = (int)((float)a_value / (float)rangeAttribute.Step);
                        a_value = steps * (float)rangeAttribute.Step;
                    }*/

                    a_value = (float)Mathf.Clamp((float)a_value, rangeAttribute.Min, rangeAttribute.Max);
                }
            }

            return a_value;
        }


        private void LoadValue()
        {
            if (!m_savesAndLoads)
            {
                return;
            }

            if (!PlayerPrefs.HasKey(SaveKey))
            {
                return;
            }

            if (Type == typeof(bool))
            {
                Value = PlayerPrefs.GetInt(SaveKey, 0) == 1;
                return;
            }

            if (Type == typeof(int))
            {
                Value = ConstrainValue(PlayerPrefs.GetInt(SaveKey, 0));
                return;
            }

            if (Type == typeof(float))
            {
                Value = ConstrainValue(PlayerPrefs.GetFloat(SaveKey, 0.0f));
                return;
            }

            UnityEngine.Debug.LogError("Failed to load value. Incorrect type;");
        }


        private void SaveValue()
        {
            if (!m_savesAndLoads)
            {
                return;
            }

            if (Type == typeof(bool))
            {
                PlayerPrefs.SetInt(SaveKey, ((bool)Value) ? 1 : 0);
                return;
            }

            if (Type == typeof(int))
            {
                PlayerPrefs.SetInt(SaveKey, (int)Value);
                return;
            }

            if (Type == typeof(float))
            {
                PlayerPrefs.SetFloat(SaveKey, (float)Value);
                return;
            }

            UnityEngine.Debug.LogError("Failed to save value. Incorrect type;");
        }


        public T GetAttribute<T>() where T : Attribute
        {
            if (m_field != null)
            {
                return DebugManager.GetAttribute<T>(m_field);
            }

            if (m_property != null)
            {
                return DebugManager.GetAttribute<T>(m_property);
            }

            return null;
        }
    }
}