﻿using System.Reflection;


namespace Defiant.Debug
{
    public class DebugMethod
    {
        public string Name { get; private set; }
        public string Description { get; private set; }

        private MethodInfo m_method;


        public DebugMethod(string a_name, string a_description, MethodInfo a_method)
        {
            Name = a_name;
            Description = a_description;
            m_method = a_method;
        }


        public void Invoke()
        {
            m_method.Invoke(null, null);
        }
    }
}