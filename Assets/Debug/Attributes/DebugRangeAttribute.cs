﻿using System;

namespace Defiant.Debug
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class DebugRangeAttribute : Attribute
    {
        public float Min { get; private set; }
        public float Max { get; private set; }
        public float Step { get; private set; }
        public bool UsesStep { get; private set; }


        public DebugRangeAttribute(float a_min, float a_max)
        {
            Min = a_min;
            Max = a_max;
            Step = 1.0f;
            UsesStep = false;
        }


        public DebugRangeAttribute(float a_min, float a_max, float a_step)
        {
            Min = a_min;
            Max = a_max;
            Step = a_step;
            UsesStep = true;
        }
    }
}
