﻿using System;

namespace Defiant.Debug
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Method)]
    public class DebugAttribute : Attribute
    {
        public string Name { get; private set; }
        public string Description { get; private set; }

        public DebugAttribute()
        {
            Name = null;
            Description = null;
        }

        public DebugAttribute(string a_name)
        {
            Name = a_name;
            Description = null;
        }

        public DebugAttribute(string a_name, string a_description)
        {
            Name = a_name;
            Description = a_description;
        }
    }
}
