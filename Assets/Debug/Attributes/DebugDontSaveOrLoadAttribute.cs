﻿using System;

namespace Defiant.Debug
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class DebugDontSaveOrLoadAttribute : Attribute
    {
        public DebugDontSaveOrLoadAttribute()
        {
        }
    }
}
