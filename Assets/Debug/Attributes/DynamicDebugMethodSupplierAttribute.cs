﻿using System;

namespace Defiant.Debug
{
    [AttributeUsage(AttributeTargets.Method)]
    public class DynamicDebugMethodSupplierAttribute : Attribute
    {
        public string Category { get; private set; }
        public string Description { get; private set; }

        public DynamicDebugMethodSupplierAttribute()
        {
            Category = null;
            Description = null;
        }

        public DynamicDebugMethodSupplierAttribute(string a_category)
        {
            Category = a_category;
            Description = null;
        }
    }
}
