﻿using System;

namespace Defiant.Debug
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Method)]
    public class DebugCategoryAttribute : Attribute
    {
        public string Category { get; private set; }

        public DebugCategoryAttribute(string a_category)
        {
            Category = a_category;
        }
    }
}
