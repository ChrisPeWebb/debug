﻿using System;

namespace Defiant.Debug
{
    [AttributeUsage(AttributeTargets.Class)]
    public class DebugWindowAttribute : Attribute
    {
        public string Name { get; private set; }
        public int Order { get; private set; }


        public DebugWindowAttribute()
        {
            Name = null;
            Order = 0;
        }


        public DebugWindowAttribute(string a_name)
        {
            Name = a_name;
            Order = 0;
        }


        public DebugWindowAttribute(string a_name, int a_order)
        {
            Name = a_name;
            Order = a_order;
        }
    }
}
