﻿//using InControl;
using UnityEngine;


namespace Defiant.Debug.GUI
{
    [RequireComponent(typeof(DebugGUI))]
    public class DebugGUIControllerInput : MonoBehaviour
    {
        [SerializeField]
        private float m_holdDownRepeatTime = 0.05f;

        [SerializeField]
        private float m_initialHoldDownTime = 0.25f;


        private DebugGUI m_debugGUI;

        //private InputControl m_previousControl = null;
        private float m_elapsedDownTime = 0.0f;


        private void Awake()
        {
            m_debugGUI = GetComponent<DebugGUI>();

            if(m_debugGUI == null)
            {
                enabled = false;
                return;
            }
        }


        private void Update()
        {
            // Handle input and send action to debug gui
            /*
            bool buttonWasPressed = false;

            if (GetControlRepeat(InControl.InputManager.ActiveDevice.DPadUp, ref buttonWasPressed))
            {
                m_debugGUI.SupplyInputAction(DebugGUIInputAction.Up);
            }

            if (GetControlRepeat(InControl.InputManager.ActiveDevice.DPadDown, ref buttonWasPressed))
            {
                m_debugGUI.SupplyInputAction(DebugGUIInputAction.Down);
            }

            if (GetControlRepeat(InControl.InputManager.ActiveDevice.DPadLeft, ref buttonWasPressed))
            {
                m_debugGUI.SupplyInputAction(DebugGUIInputAction.Left);
            }

            if (GetControlRepeat(InControl.InputManager.ActiveDevice.DPadRight, ref buttonWasPressed))
            {
                m_debugGUI.SupplyInputAction(DebugGUIInputAction.Right);
            }

            if (InControl.InputManager.ActiveDevice.LeftStickButton.WasPressed &&
                InControl.InputManager.ActiveDevice.RightStickButton.WasPressed)
            {
                m_debugGUI.SupplyInputAction(DebugGUIInputAction.OpenClose);
            }
            else if (InControl.InputManager.ActiveDevice.LeftStickButton.WasPressed &&
                 InControl.InputManager.ActiveDevice.RightStickButton.IsPressed)
            {
                m_debugGUI.SupplyInputAction(DebugGUIInputAction.OpenClose);
            }
            else if (InControl.InputManager.ActiveDevice.LeftStickButton.IsPressed &&
                    InControl.InputManager.ActiveDevice.RightStickButton.WasPressed)
            {
                m_debugGUI.SupplyInputAction(DebugGUIInputAction.OpenClose);
            }

            if (GetControlRepeat(InControl.InputManager.ActiveDevice.Action1, ref buttonWasPressed))
            {
                m_debugGUI.SupplyInputAction(DebugGUIInputAction.Activate);
            }

            if (GetControlRepeat(InControl.InputManager.ActiveDevice.RightBumper, ref buttonWasPressed))
            {
                m_debugGUI.SupplyInputAction(DebugGUIInputAction.NextCategory);
            }

            if (GetControlRepeat(InControl.InputManager.ActiveDevice.LeftBumper, ref buttonWasPressed))
            {
                m_debugGUI.SupplyInputAction(DebugGUIInputAction.PreviousCategory);
            }

            if (!buttonWasPressed)
            {
                m_previousControl = null;
                m_elapsedDownTime = 0.0f;
            }*/
        }

        /*
        private bool GetControlRepeat(InputControl a_inputControl, ref bool a_wasPressed)
        {
            if (a_inputControl.IsPressed)
            {
                a_wasPressed = true;

                if (m_previousControl != a_inputControl)
                {
                    m_previousControl = a_inputControl;
                    return true;
                }

                m_previousControl = a_inputControl;

                m_elapsedDownTime += Time.unscaledDeltaTime;

                if (m_elapsedDownTime > m_initialHoldDownTime)
                {
                    if (m_elapsedDownTime > m_holdDownRepeatTime)
                    {
                        m_elapsedDownTime -= m_holdDownRepeatTime;
                        return true;
                    }
                }
            }

            return false;
        }*/
    }
}
