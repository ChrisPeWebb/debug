﻿using System;
using UnityEngine;
using System.Collections.Generic;

namespace Defiant.Debug.GUI
{
    /// <summary>
    /// An IDebugWindow implementation that automatically handles the drawing and input of a list of controls
    /// Also handles a scrollbar if needed
    /// </summary>
    public abstract class BaseDebugWindow : IDebugWindow
    {
        private List<BaseDebugControl> m_controls = new List<BaseDebugControl>();
        private List<BaseDebugControl> m_toolbarControls = new List<BaseDebugControl>();
        private int m_currentlySelectedControlIndex = 0;
        private Vector2 m_scrollPosition = Vector2.zero;
        private float m_scrollViewHeight = 0.0f;
        private bool m_selectedControlChanged = false;

        protected List<BaseDebugControl> Controls
        {
            get { return m_controls; }
            set { m_controls = value; }
        }

        /// <summary>
        /// Toolbar controls are similar to regular controls, but are always drawn first, and are not drawn in the scrollview
        /// </summary>
        protected List<BaseDebugControl> ToolbarControls
        {
            get { return m_toolbarControls; }
            set { m_toolbarControls = value; }
        }

        protected int CurrentlySelectedControlIndex
        {
            get { return m_currentlySelectedControlIndex; }
            set
            {
                m_currentlySelectedControlIndex = value;
                m_selectedControlChanged = true;
            }
        }

        private int TotalControlCount
        {
            get
            {
                return ((ToolbarControls != null) ? ToolbarControls.Count : 0) + 
                    ((Controls != null) ? Controls.Count : 0);
            }
        }

        protected BaseDebugControl CurrentlySelectedControl
        {
            get
            {
                if (ToolbarControls.Count > 0 && m_currentlySelectedControlIndex < ToolbarControls.Count)
                    return ToolbarControls[m_currentlySelectedControlIndex];

                if (Controls.Count > 0 && m_currentlySelectedControlIndex - ToolbarControls.Count < Controls.Count)
                    return Controls[m_currentlySelectedControlIndex - ToolbarControls.Count];

                return null;
            }
        }


        // TODO: Optimize drawing by skipping controls that are well off screen.
        // This might be difficult because of largly varying control height
        public void Draw(bool a_isSelected)
        {
            ClampCurrentlySelectedControl();

            if (m_toolbarControls != null)
            {
                for (int i = 0; i < m_toolbarControls.Count; ++i)
                {
                    m_toolbarControls[i].Draw(CurrentlySelectedControlIndex == i && a_isSelected);
                }
            }

            float totalContentsHeight = 0.0f;

            if (m_controls != null)
            {
                m_scrollPosition = GUILayout.BeginScrollView(m_scrollPosition);
                {
                    for (int i = 0; i < m_controls.Count; ++i)
                    {
                        m_controls[i].Draw(CurrentlySelectedControlIndex == i + ToolbarControls.Count && a_isSelected);

                        if (Event.current.type == EventType.Repaint)
                        {
                            // Accumulate height of all controls being drawn
                            totalContentsHeight += GUILayoutUtility.GetLastRect().height;

                            if (CurrentlySelectedControlIndex == i + ToolbarControls.Count && a_isSelected && m_selectedControlChanged)
                            {
                                // Center on control
                                // NOTE: Minus half scroll height so that selected control is in the center of scroll view
                                m_scrollPosition.y = totalContentsHeight - 
                                    m_scrollViewHeight * 0.5f - 
                                    GUILayoutUtility.GetLastRect().height * 0.5f;

                                m_selectedControlChanged = false;
                            }
                        }
                    }
                }
                GUILayout.EndScrollView();

                // Update scrollview height in event of window resize
                if (Event.current.type == EventType.Repaint)
                    m_scrollViewHeight = GUILayoutUtility.GetLastRect().height;
            }
        }

        // TODO: Currently, if a control is selected then becomes non selectable, it will never be unselected. NOTE: I think this has been fixed. Please confirm that
        public bool HandleInput(DebugGUIInputAction a_inputAction)
        {
            if (TotalControlCount == 0)
                return false;

            ClampCurrentlySelectedControl();

            if (CurrentlySelectedControl.Selectable())
            {
                CurrentlySelectedControl.HandleInput(a_inputAction);
            }
            else // If the currently selected control is not selectable, we want to unselect it
            {
                // Skip controls which are not selectable
                while (CurrentlySelectedControlIndex >= 0 && !CurrentlySelectedControl.Selectable())
                {
                    --CurrentlySelectedControlIndex;
                }

                if (CurrentlySelectedControlIndex < 0)
                {
                    CurrentlySelectedControlIndex = 0;
                    return false;
                }

                a_inputAction = DebugGUIInputAction.None;
            }

            if (a_inputAction == DebugGUIInputAction.Up)
            {
                --CurrentlySelectedControlIndex;

                // Skip controls which are not selectable
                while(CurrentlySelectedControlIndex >= 0 && !CurrentlySelectedControl.Selectable())
                {
                    --CurrentlySelectedControlIndex;
                }

                if (CurrentlySelectedControlIndex < 0)
                {
                    CurrentlySelectedControlIndex = 0;
                    return false;
                }

                a_inputAction = DebugGUIInputAction.None;
            }

            if (a_inputAction == DebugGUIInputAction.Down)
            {
                ++CurrentlySelectedControlIndex;

                // Skip controls which are not selectable
                while (CurrentlySelectedControlIndex <= TotalControlCount - 1 && !CurrentlySelectedControl.Selectable())
                {
                    ++CurrentlySelectedControlIndex;
                }

                if (CurrentlySelectedControlIndex > TotalControlCount - 1)
                {
                    CurrentlySelectedControlIndex = TotalControlCount - 1;
                }

                a_inputAction = DebugGUIInputAction.None;
            }

            return true;
        }


        private void ClampCurrentlySelectedControl()
        {
            CurrentlySelectedControlIndex = Mathf.Clamp(m_currentlySelectedControlIndex, 0, TotalControlCount - 1);
        }


        public virtual void OnSelected()
        {
            CurrentlySelectedControlIndex = 0;
        }


        public abstract void Initialize();

        public abstract bool IsAvailable();

        public abstract void Update(bool a_isSelected);
    }
}
