﻿using UnityEngine;
using System;
using System.Collections;

namespace Defiant.Debug.GUI
{
    public class DebugCategoryControl : BaseDebugControl
    {
        public Action<int> OnCategoryChanged;
        public int CurrentCategory { get; private set; }
        private string[] m_categories; 


        public DebugCategoryControl(string[] a_categories)
        {
            CurrentCategory = 0;
            m_categories = a_categories;
        }


        public void SetCategories(string[] a_categories)
        {
            m_categories = a_categories;
        }


        protected override void DrawInternal()
        {
            GUILayout.BeginHorizontal();
            int newCategory = GUILayout.Toolbar(CurrentCategory, m_categories);

            if(newCategory != CurrentCategory)
            {
                CurrentCategory = newCategory;
                if (OnCategoryChanged != null)
                {
                    OnCategoryChanged(CurrentCategory);
                }
            }
            GUILayout.EndHorizontal();
        }


        protected override void HandleInputInternal(DebugGUIInputAction a_inputAction)
        {
            bool categoryChanged = false;

            if (a_inputAction == DebugGUIInputAction.Left || a_inputAction == DebugGUIInputAction.PreviousPage)
            {
                --CurrentCategory;

                if(CurrentCategory < 0)
                {
                    CurrentCategory = m_categories.Length - 1;
                }

                categoryChanged = true;
            }

            if (a_inputAction == DebugGUIInputAction.Right || a_inputAction == DebugGUIInputAction.NextPage)
            {
                ++CurrentCategory;

                if (CurrentCategory >= m_categories.Length)
                {
                    CurrentCategory = 0;
                }

                categoryChanged = true;
            }

            if (categoryChanged)
            {
                if (OnCategoryChanged != null)
                {
                    OnCategoryChanged(CurrentCategory);
                }
            }
        }
    }
}
