﻿using System.Text.RegularExpressions;
using UnityEngine;


namespace Defiant.Debug.GUI
{
    public class DebugFloatValueControl : BaseDebugControl
    {
        private DebugValue m_value;

        private string m_valueString = "";

        public DebugFloatValueControl(DebugValue a_value)
        {
            m_value = a_value;

            m_valueString = ((float)m_value.Value).ToString();
        }


        protected override void DrawInternal()
        {
            GUILayout.BeginHorizontal();

            float originalValue = (float)m_value.Value;

            // TODO: This is still pretty bad. Its difficult to input decimal places

            m_valueString = GUILayout.TextField(m_valueString, GUILayout.Width(45.0f));
            GUILayout.Label(m_value.Name, GUILayout.Width(300.0f));
            m_valueString = Regex.Replace(m_valueString, @"[^\d -.]", "");

            float newValue;

            if (float.TryParse(m_valueString, out newValue))
            {
                if (originalValue != newValue)
                {
                    m_value.Value = newValue;

                    // Requires resetting value string in the event that value setting changed value
                    m_valueString = ((float)m_value.Value).ToString();
                }
            }
            else if (m_valueString.Equals(""))
            {
            }

            GUILayout.Space(10.0f);

            Color originalColor = UnityEngine.GUI.color;
            UnityEngine.GUI.color = new Color(0.75f, 0.75f, 0.75f);
            GUILayout.Label(m_value.Description);
            UnityEngine.GUI.color = originalColor;

            GUILayout.EndHorizontal();
        }


        protected override void HandleInputInternal(DebugGUIInputAction a_inputAction)
        {
            if (a_inputAction == DebugGUIInputAction.Left)
            {
                m_value.StepDown();
                m_valueString = ((float)m_value.Value).ToString();
            }

            if (a_inputAction == DebugGUIInputAction.Right)
            {
                m_value.StepUp();
                m_valueString = ((float)m_value.Value).ToString();
            }
        }
    }
}
