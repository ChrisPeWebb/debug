﻿using System;
using UnityEngine;


namespace Defiant.Debug.GUI
{
    public class DebugBoolValueControl : BaseDebugControl
    {
        private DebugValue m_value;

        public DebugBoolValueControl(DebugValue a_value)
        {
            m_value = a_value;
        }


        protected override void DrawInternal()
        {
            GUILayout.BeginHorizontal();

            bool originalValue = (bool)m_value.Value;

            bool newValue = GUILayout.Toggle(originalValue, m_value.Name, GUILayout.Width(350.0f));

            if (originalValue != newValue)
            {
                m_value.Value = newValue;
            }

            GUILayout.Space(10.0f);

            Color originalColor = UnityEngine.GUI.color;
            UnityEngine.GUI.color = new Color(0.75f, 0.75f, 0.75f);
            GUILayout.Label(m_value.Description);
            UnityEngine.GUI.color = originalColor;

            GUILayout.EndHorizontal();
        }


        protected override void HandleInputInternal(DebugGUIInputAction a_inputAction)
        {
            if (a_inputAction == DebugGUIInputAction.Activate ||
                a_inputAction == DebugGUIInputAction.Left ||
                a_inputAction == DebugGUIInputAction.Right)
            {
                m_value.Value = !(bool)m_value.Value;
            }
        }
    }
}
