﻿using UnityEngine;


namespace Defiant.Debug.GUI
{
    public class DynamicDebugMethodControl : BaseDebugControl
    {
        private DynamicDebugMethod m_method;

        public DynamicDebugMethodControl(DynamicDebugMethod a_method)
        {
            m_method = a_method;
        }


        protected override void DrawInternal()
        {
            GUILayout.BeginHorizontal();
            if(GUILayout.Button(m_method.Name, GUILayout.Width(350.0f)))
            {
                Invoke();
            }

            GUILayout.Space(10.0f);

            Color originalColor = UnityEngine.GUI.color;
            UnityEngine.GUI.color = new Color(0.75f, 0.75f, 0.75f);
            GUILayout.Label(m_method.Description);
            UnityEngine.GUI.color = originalColor;

            GUILayout.EndHorizontal();
        }


        protected override void HandleInputInternal(DebugGUIInputAction a_inputAction)
        {
            if (a_inputAction == DebugGUIInputAction.Activate)
            {
                Invoke();
            }
        }


        private void Invoke()
        {
            m_method.Action.Invoke();
        }
    }
}
