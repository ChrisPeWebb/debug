﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace Defiant.Debug.GUI
{
    public class DebugWindowTabControl : BaseDebugControl
    {
        public Action<int> OnWindowChanged;
        public int CurrentWindow { get; private set; }
        private List<KeyValuePair<DebugWindowAttribute, IDebugWindow>> m_windows;
        private List<string> m_availableWindowNames = new List<string>(10);
        private List<IDebugWindow> m_availableWindows = new List<IDebugWindow>(10);
        private IDebugWindow m_currentWindow = null;

        public DebugWindowTabControl(List<KeyValuePair<DebugWindowAttribute, IDebugWindow>> a_windows)
        {
            CurrentWindow = 0;
            m_windows = a_windows;

            UpdateAvailableWindows();
        }


        protected override void DrawInternal()
        {
            UpdateAvailableWindows();

            GUILayout.BeginHorizontal();
            int newWindow = GUILayout.Toolbar(CurrentWindow, m_availableWindowNames.ToArray());

            if(newWindow != CurrentWindow)
            {
                CurrentWindow = newWindow;
                if (OnWindowChanged != null)
                {
                    OnWindowChanged(CurrentWindow);
                }
            }
            GUILayout.EndHorizontal();
        }


        protected override void HandleInputInternal(DebugGUIInputAction a_inputAction)
        {
            bool categoryChanged = false;

            if (a_inputAction == DebugGUIInputAction.Left || a_inputAction == DebugGUIInputAction.PreviousPage)
            {
                --CurrentWindow;
                categoryChanged = true;
            }

            if (a_inputAction == DebugGUIInputAction.Right || a_inputAction == DebugGUIInputAction.NextPage)
            {
                ++CurrentWindow;
                categoryChanged = true;
            }

            if (categoryChanged)
            {
                ClampCurrentWindow();

                if (OnWindowChanged != null)
                {
                    OnWindowChanged(CurrentWindow);
                }
            }
        }


        private void UpdateAvailableWindows()
        {
            m_availableWindowNames.Clear();
            m_availableWindows.Clear();

            for (int i = 0; i < m_windows.Count; ++i)
            {
                if (m_windows[i].Value.IsAvailable())
                {
                    m_availableWindowNames.Add(m_windows[i].Key.Name);
                    m_availableWindows.Add(m_windows[i].Value);
                }
            }

            ClampCurrentWindow();

            if (m_availableWindows.Count == 0)
                return;

            if(m_currentWindow != m_availableWindows[CurrentWindow])
            {
                m_currentWindow = m_availableWindows[CurrentWindow];

                if (OnWindowChanged != null)
                {
                    OnWindowChanged(CurrentWindow);
                }
            }
        }


        private void ClampCurrentWindow()
        {
            if (CurrentWindow < 0)
            {
                CurrentWindow = m_availableWindowNames.Count - 1;
            }
            else if (CurrentWindow >= m_availableWindowNames.Count)
            {
                CurrentWindow = 0;
            }
        }
    }
}
