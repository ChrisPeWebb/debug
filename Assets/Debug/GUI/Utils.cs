﻿using System.Text;

namespace Defiant.Debug.GUI
{
    public static class Utils
    {
        public static string GetBytesReadable(long i)
        {
            var sign = (i < 0 ? "-" : "");
            double readable = (i < 0 ? -i : i);
            string suffix;
            if (i >= 0x1000000000000000) // Exabyte
            {
                suffix = "EB";
                readable = i >> 50;
            }
            else if (i >= 0x4000000000000) // Petabyte
            {
                suffix = "PB";
                readable = i >> 40;
            }
            else if (i >= 0x10000000000) // Terabyte
            {
                suffix = "TB";
                readable = i >> 30;
            }
            else if (i >= 0x40000000) // Gigabyte
            {
                suffix = "GB";
                readable = i >> 20;
            }
            else if (i >= 0x100000) // Megabyte
            {
                suffix = "MB";
                readable = i >> 10;
            }
            else if (i >= 0x400) // Kilobyte
            {
                suffix = "KB";
                readable = i;
            }
            else
            {
                return i.ToString(sign + "0 B"); // Byte
            }
            readable /= 1024;

            return sign + readable.ToString("0.### ") + suffix;
        }

        public static string NicifyVariableName(string a_variableName)
        {
            if (a_variableName == null)
                return "";

            // Capitalize first letter
            if (a_variableName.Length == 1)
            {
                a_variableName = char.ToUpper(a_variableName[0]).ToString();
            }
            else if (a_variableName.Length > 1)
            {
                a_variableName = char.ToUpper(a_variableName[0]) + a_variableName.Substring(1);
            }

            // Convert underscores to spaces
            a_variableName.Replace('_', ' ');
            a_variableName.Replace('-', ' ');

            // Add spaces between camel case words
            a_variableName = AddSpacesToSentence(a_variableName, true);

            return a_variableName;
        }


        private static string AddSpacesToSentence(string a_text, bool a_preserveAcronyms)
        {
            if (string.IsNullOrEmpty(a_text) || a_text.Trim().Length == 0)  // Equivilent to IsNullOrWhiteSpace in modern .net
            {
                return string.Empty;
            }

            StringBuilder newText = new StringBuilder(a_text.Length * 2);
            newText.Append(a_text[0]);

            for (int i = 1; i < a_text.Length; i++)
            {
                if (char.IsUpper(a_text[i]))
                {
                    if ((a_text[i - 1] != ' ' && !char.IsUpper(a_text[i - 1])) ||
                        (a_preserveAcronyms && char.IsUpper(a_text[i - 1]) &&
                         i < a_text.Length - 1 && !char.IsUpper(a_text[i + 1])))
                    {
                        newText.Append(' ');
                    }
                }
                newText.Append(a_text[i]);
            }

            return newText.ToString();
        }
    }
}
