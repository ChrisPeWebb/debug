﻿using UnityEngine;

namespace Defiant.Debug.GUI
{
    public abstract class BaseDebugControl
    {
        public void Draw(bool a_isSelected)
        {
            // Draw a box around the control highlighting it
            Color currentColor = UnityEngine.GUI.backgroundColor;

            if (a_isSelected)
            {
                UnityEngine.GUI.backgroundColor = Color.white;
            }
            else
            {
                UnityEngine.GUI.backgroundColor = Color.clear;
            }

            GUILayout.BeginVertical("U2D.createRect");

            UnityEngine.GUI.backgroundColor = currentColor;

            // Draw the control
            DrawInternal();

            GUILayout.EndVertical();
        }


        public void HandleInput(DebugGUIInputAction a_inputAction)
        {
            HandleInputInternal(a_inputAction);
        }


        protected abstract void DrawInternal();
        protected abstract void HandleInputInternal(DebugGUIInputAction a_inputAction);


        // TODO: Make this functional. If a control is not selectable and is selected, push control past it
        public virtual bool Selectable()
        {
            return true;
        }
    }
}
