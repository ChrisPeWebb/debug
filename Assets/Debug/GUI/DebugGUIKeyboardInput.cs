﻿using UnityEngine;


namespace Defiant.Debug.GUI
{
    [RequireComponent(typeof(DebugGUI))]
    public class DebugGUIKeyboardInput : MonoBehaviour
    {
        [SerializeField]
        private KeyCode m_upKeyCode = KeyCode.UpArrow;

        [SerializeField]
        private KeyCode m_downKeyCode = KeyCode.DownArrow;

        [SerializeField]
        private KeyCode m_leftKeyCode = KeyCode.LeftArrow;

        [SerializeField]
        private KeyCode m_rightKeyCode = KeyCode.RightArrow;

        [SerializeField]
        private KeyCode m_nextCategoryKeyCode = KeyCode.PageUp;

        [SerializeField]
        private KeyCode m_previousCategoryKeyCode = KeyCode.PageDown;

        [SerializeField]
        private KeyCode m_openCloseKeyCode = KeyCode.BackQuote;

        [SerializeField]
        private KeyCode m_activateKeyCode = KeyCode.Return;

        [SerializeField]
        private float m_holdDownRepeatTime = 0.05f;

        [SerializeField]
        private float m_initialHoldDownTime = 0.25f;

        private DebugGUI m_debugGUI;

        private KeyCode m_previousKey = KeyCode.None;
        private float m_elapsedDownTime = 0.0f;


        private void Awake()
        {
            m_debugGUI = GetComponent<DebugGUI>();

            if(m_debugGUI == null)
            {
                enabled = false;
                return;
            }
        }


        private void Update()
        {
            if (!Input.anyKey)
            {
                m_previousKey = KeyCode.None;
                m_elapsedDownTime = 0.0f;
            }

            if (GetKeyRepeat(m_upKeyCode))
            {
                m_debugGUI.SupplyInputAction(DebugGUIInputAction.Up);
            }

            if (GetKeyRepeat(m_downKeyCode))
            {
                m_debugGUI.SupplyInputAction(DebugGUIInputAction.Down);
            }

            if (GetKeyRepeat(m_leftKeyCode))
            {
                m_debugGUI.SupplyInputAction(DebugGUIInputAction.Left);
            }

            if (GetKeyRepeat(m_rightKeyCode))
            {
                m_debugGUI.SupplyInputAction(DebugGUIInputAction.Right);
            }

            if (GetKeyRepeat(m_openCloseKeyCode))
            {
                m_debugGUI.SupplyInputAction(DebugGUIInputAction.OpenClose);
            }

            if (GetKeyRepeat(m_activateKeyCode))
            {
                m_debugGUI.SupplyInputAction(DebugGUIInputAction.Activate);
            }

            if (GetKeyRepeat(m_nextCategoryKeyCode))
            {
                m_debugGUI.SupplyInputAction(DebugGUIInputAction.NextPage);
            }

            if (GetKeyRepeat(m_previousCategoryKeyCode))
            {
                m_debugGUI.SupplyInputAction(DebugGUIInputAction.PreviousPage);
            }
        }


        private bool GetKeyRepeat(KeyCode a_key)
        {
            if (Input.GetKey(a_key))
            {
                if (m_previousKey != a_key)
                {
                    m_previousKey = a_key;
                    return true;
                }

                m_previousKey = a_key;

                m_elapsedDownTime += Time.unscaledDeltaTime;

                if (m_elapsedDownTime > m_initialHoldDownTime)
                {
                    if (m_elapsedDownTime > m_holdDownRepeatTime)
                    {
                        m_elapsedDownTime -= m_holdDownRepeatTime;
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
