﻿using System;
using System.Collections.Generic;
using UnityEngine;


namespace Defiant.Debug.GUI
{
    public enum DebugGUIInputAction
    {
        OpenClose           = 0,
        Up                  = 1,
        Down                = 2,
        Left                = 3,
        Right               = 4,
        NextPage            = 5,
        PreviousPage        = 6,
        Activate            = 7,
        None                = 8
    }


    public class DebugGUI : MonoBehaviour
    {
        /// <summary>
        /// Called when GUI state changes. True if debug gui is enabled, else False
        /// </summary>
        public static Action<bool> EnabledStateChanged;

        [SerializeField]
        private GUISkin m_skin;

        [SerializeField]
        private bool m_persistent;

        private DebugWindowTabControl m_windowTabControl;
        private List<KeyValuePair<DebugWindowAttribute, IDebugWindow>> m_windows;
        private List<IDebugWindow> m_availableWindows = new List<IDebugWindow>(10);

        private int m_currentlySelectedWindow = 0;
        private DebugGUIInputAction m_lastInputAction = DebugGUIInputAction.None;
        private bool m_isEnabled = false;
        private bool m_isWindowSelected = true;

        private Rect m_windowRect;

        // TODO: Allow window to steal selection
        // TODO: Clean up how windows become available. Ideally this and DebugWindowTabControl shouldnt handle the same thing

        private void Start()
        {
            // For Testing
            m_isEnabled = true;
            //m_currentlySelectedWindow = 2;

            if (DebugManager.Instance == null)
            {
                enabled = false;
                return;
            }

            if(m_persistent)
            {
                DontDestroyOnLoad(this.gameObject);
            }

            m_windows = DebugManager.Instance.DebugWindows;

            UpdateAvailableWindows();

            m_windowTabControl = new DebugWindowTabControl(m_windows);
            m_windowTabControl.OnWindowChanged += OnWindowChanged;

            if (m_windows != null)
            {
                for (int i = 0; i < m_windows.Count; ++i)
                {
                    m_windows[i].Value.Initialize();
                }
            }
        }


        public void SupplyInputAction(DebugGUIInputAction a_inputAction)
        {
            m_lastInputAction = a_inputAction;
        }


        private void Update()
        {
            if (m_windows == null)
                return;

            UpdateAvailableWindows();

            for (int i = 0; i < m_availableWindows.Count; ++i)
            {
                m_availableWindows[i].Update(i == m_currentlySelectedWindow);
            }
        }


        private void OnGUI()
        {
            if (m_windows == null)
                return;

            HandleInput();

            if (!m_isEnabled)
                return;

            if (m_skin != null)
            {
                UnityEngine.GUI.skin = m_skin;
            }

            float xPadding = 20.0f;
            float yPadding = 50.0f;
            m_windowRect = new Rect(xPadding, yPadding, Screen.width - xPadding * 2.0f, Screen.height - yPadding * 2.0f);

            UnityEngine.GUI.Window(0, m_windowRect, DrawWindow, "Debug Menu");
        }


        void DrawWindow(int windowID)
        {
            GUILayout.BeginVertical("TextArea");
            {
                m_windowTabControl.HandleInput(m_lastInputAction);
                m_windowTabControl.Draw(!m_isWindowSelected);

                GUILayout.BeginVertical("AnimationCurveEditorBackground", GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));

                if (m_availableWindows.Count > m_currentlySelectedWindow)
                {
                    m_availableWindows[m_currentlySelectedWindow].Draw(m_isWindowSelected);
                }

                GUILayout.EndVertical();
            }
            GUILayout.EndVertical();
            
            // Consume input action
            m_lastInputAction = DebugGUIInputAction.None;
        }


        private void OnWindowChanged(int m_newWindow)
        {
            if (DebugManager.Instance == null || m_availableWindows.Count <= m_newWindow || m_newWindow < 0)
            {
                return;
            }

            UpdateAvailableWindows();

            m_currentlySelectedWindow = m_newWindow;

            m_availableWindows[m_currentlySelectedWindow].OnSelected();
        }

        
        private void HandleInput()
        {
            if(m_lastInputAction == DebugGUIInputAction.OpenClose)
            {
                m_isEnabled = !m_isEnabled;

                if(EnabledStateChanged != null)
                {
                    EnabledStateChanged(m_isEnabled);
                }

                m_lastInputAction = DebugGUIInputAction.None;
            }

            if (m_availableWindows.Count <= m_currentlySelectedWindow)
                return;

            if(m_isWindowSelected)
            {
                m_isWindowSelected = m_availableWindows[m_currentlySelectedWindow].HandleInput(m_lastInputAction);
                m_lastInputAction = DebugGUIInputAction.None;
            }

            if (m_lastInputAction == DebugGUIInputAction.Down)
            {
                if (!m_isWindowSelected)
                {
                    m_isWindowSelected = true;
                    m_availableWindows[m_currentlySelectedWindow].OnSelected();
                }
                m_lastInputAction = DebugGUIInputAction.None;
            }
        }


        private void UpdateAvailableWindows()
        {
            m_availableWindows.Clear();

            for (int i = 0; i < m_windows.Count; ++i)
            {
                if (m_windows[i].Value.IsAvailable())
                {
                    m_availableWindows.Add(m_windows[i].Value);
                }
            }
        }
    }
}
