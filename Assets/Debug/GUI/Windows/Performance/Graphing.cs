﻿using UnityEngine;
using System.Collections.Generic;

namespace Defiant.Debug.GUI
{
    public enum GraphType
    {
        Bar = 0,
        Line = 1
    }


    public struct GraphLevelLabel
    {
        public string Label { get; set; }
        public float Level { get; set; }
        public Color Color { get; set; }

        public GraphLevelLabel(string a_label, float a_level, Color? a_color = null)
        {
            Label = a_label;
            Level = a_level;
            Color = a_color ?? new Color(0.8f, 0.8f, 0.8f, 0.5f);
        }
    }

    public static class Graphing
    {
        private static Material s_lineMaterial;
        private static Material LineMaterial
        {
            get
            {
                if(s_lineMaterial == null)
                {
                    Shader shader = Shader.Find("Unlit/Color");
                    if (shader == null)
                    {
                        UnityEngine.Debug.LogError("Please add Unlit/Color shader to Always Include Shader in graphics settings. Required for graph drawing.");
                        shader = Shader.Find("UI/Default"); // One of the default included unity shaders. shouldnt break....right?
                        // NOTE: This will not draw correctly. Only done to prevent mass error spam
                    }

                    s_lineMaterial = new Material(shader);
                }
                return s_lineMaterial;
            }
        }

        // TODO: Perhaps we want graph classes which can be drawn? This way we can cache stuff like verts

        // TODO: Current value label is a bit crap for most graphs
        // TODO: Currently graphs break when drawn outside a window/scrollview. Fix this
        // TODO: Allow to take several data sets
        // TODO: Better framing of graph line. Calc min value and normalize around 50%?
        public static void DrawGraph(string a_name, GraphType a_graphType, int a_resolution, GraphLevelLabel[] a_levelLabels, bool a_displayCurrentValue, List<float> a_graphData)
        {
            if (!string.IsNullOrEmpty(a_name))
            {
                GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                GUILayout.Label(a_name, "ObjectPickerLargeStatus");
                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();
                GUILayout.Space(-7.0f);
            }

            if (a_graphData == null || a_graphData.Count == 0)
            {
                GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                GUILayout.Label("No Data");
                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();
                return;
            }

            // Get a region to draw ourselves in
            //GUILayout.BeginVertical("flow background", GUILayout.Height(200.0f), GUILayout.ExpandWidth(true));
            GUILayout.BeginVertical("flow background", GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
            //GUILayout.BeginVertical("flow background", GUILayout.MinHeight(150.0f), GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();

            Rect rect = GUILayoutUtility.GetLastRect();

            float width = rect.width;
            float spacePerSample = Mathf.Max(a_resolution, 1);
            float sampleWidth = Mathf.Max(spacePerSample - 2, 1); // 4.0f
            int resolution = 1;
            //int stride = 2; // TODO: Make this work
            int numberOfSamples = (int)(width / spacePerSample / resolution);

            numberOfSamples = Mathf.Min(numberOfSamples, a_graphData.Count);

            // TODO: Allow for sampling over longer time than we have pixels (or even skip pixels/samples).

            // Get max value for normalization
            float maxValue = float.MinValue;
            for (int i = a_graphData.Count - 1; i >= a_graphData.Count - numberOfSamples; --i)
            {
                if (a_graphData[i] > maxValue)
                {
                    maxValue = a_graphData[i];
                }
            }

            // Gives a bit of space to the data on the graph. Dont want to constrict 100%
            maxValue *= 1.25f;

            int numberOfPoints = Mathf.Max(0, (numberOfSamples - 1) * 2);

            Vector3[] m_linePoints = new Vector3[numberOfPoints];

            Vector2 currentBottomPosition = new Vector2(rect.xMax, rect.yMax);
            Vector2 previousTopPosition = Vector2.zero;

            for (int i = 0; i < numberOfSamples; ++i)
            {
                float value = a_graphData[a_graphData.Count - 1 - i];
                float normalizedTime = value / maxValue;

                Vector2 topPosition = new Vector2(currentBottomPosition.x, currentBottomPosition.y - rect.height * normalizedTime);

                if (a_graphType == GraphType.Line)
                {
                    if (i > 0)
                    {
                        m_linePoints[(i - 1) * 2] = previousTopPosition;
                        m_linePoints[(i - 1) * 2 + 1] = topPosition;
                    }
                }
                else
                {
                    Drawing.DrawLine(currentBottomPosition, topPosition, new Color(0.5f, 1.0f, 0.6f, 0.25f), sampleWidth);
                }

                currentBottomPosition.x -= spacePerSample * resolution;
                previousTopPosition = topPosition;
            }

            // Draw level lines. We only want to draw 3 (to keep it neat), so we will only draw the first 3, in order of how bad they are
            int levelsDrawn = 0;
            int maxLevels = 3;

            // Draw level labels
            if (a_levelLabels != null)
            {
                for (int i = 0; i < a_levelLabels.Length; ++i)
                {
                    GraphLevelLabel level = a_levelLabels[i];
                    if (DrawGraphLevelLabel(rect, level.Level / maxValue, ref a_levelLabels[i]))
                    {
                        ++levelsDrawn;
                    }

                    if (levelsDrawn >= maxLevels)
                    {
                        break;
                    }
                }
            }

            // Draw graph. Important this is done after drawing levels
            if (a_graphType == GraphType.Line)
            {
                if (m_linePoints.Length >= 2)
                {
                    LineMaterial.color = new Color(0.5f, 0.8f, 1.0f, 0.95f);

                    GL.PushMatrix();
                    LineMaterial.SetPass(0);
                    GL.LoadPixelMatrix();
                    // TODO: use GL.Viewport to clip to window
                    GL.Begin(GL.LINES);
                    for (int i = 0; i < m_linePoints.Length; i += 2)
                    {
                        Vector2 pointA = m_linePoints[i];
                        Vector2 pointB = m_linePoints[i + 1];

                        // HACK: 50 corresponds to the padding amount applied to the window.. Do this nicer
                        // NOTE: subbing from height because GL coords are flipped in y compared to unity gui coords
                        pointA.y = Screen.height - pointA.y - 50;
                        pointB.y = Screen.height - pointB.y - 50;

                        // HACK: 20 corresponds to the padding amount applied to the window.. Do this nicer
                        pointA.x = pointA.x + 20;
                        pointB.x = pointB.x + 20;

                        GL.Vertex(pointA);
                        GL.Vertex(pointB);
                    }
                    GL.End();
                    GL.PopMatrix();
                }
            }

            if (a_displayCurrentValue)
            {
                float currentValue = a_graphData[a_graphData.Count - 1];
                float currentAlpha = currentValue / maxValue;
                string label = currentValue.ToString("0.");

                Vector2 rightPosition = new Vector2((int)rect.xMax, (int)(rect.yMax - rect.height * currentAlpha));

                Vector2 labelSize = UnityEngine.GUI.skin.GetStyle("ProfilerBadge").CalcSize(new GUIContent(label));

                Vector2 labelPosition = rightPosition;
                labelPosition.x -= 5.0f;
                labelPosition.x -= labelSize.x;

                UnityEngine.GUI.Label(new Rect(labelPosition, labelSize), label, "ProfilerBadge");
            }
        }


        private static bool DrawGraphLevelLabel(Rect a_rect, float a_alpha, ref GraphLevelLabel a_level)
        {
            // Dont draw if the value is off the graph (with a bit of space at the top for cleanliness)
            // TODO: This doesnt really work for very small graphs, but is okay for now. Ideally we would base it on size of label and size of graph rect
            if (a_alpha > 0.95f || a_alpha < 0.0f)
                return false;

            Color color = a_level.Color;

            // Clamping coords to int makes out lines have consistent width. floating point coord lines can have size variation from antialiasing
            Vector2 leftPosition = new Vector2((int)a_rect.xMin, (int)(a_rect.yMax - a_rect.height * a_alpha));
            Vector2 rightPosition = new Vector2((int)a_rect.xMax, (int)(a_rect.yMax - a_rect.height * a_alpha));

            Vector2 leftPositionGL = leftPosition;
            Vector2 rightPositionGL = rightPosition;

            // HACK: to compensate for no trapsarency on line shader (unity has no unlit, transparent shader with color property by default)
            // we are darkening
            color.r *= color.a;
            color.g *= color.a;
            color.b *= color.a;

            LineMaterial.color = color;

            GL.PushMatrix();
            LineMaterial.SetPass(0);
            GL.LoadPixelMatrix();
            GL.Begin(GL.LINES);

            // HACK: 50 corresponds to the padding amount applied to the window.. Do this nicer
            // NOTE: subbing from height because GL coords are flipped in y compared to unity gui coords
            leftPositionGL.y = Screen.height - leftPositionGL.y - 50;
            rightPositionGL.y = Screen.height - rightPositionGL.y - 50;

            // HACK: 20 corresponds to the padding amount applied to the window.. Do this nicer
            leftPositionGL.x = leftPositionGL.x + 20;
            rightPositionGL.x = rightPositionGL.x + 20;

            // HACK: depth -1 so that levels are drawn below plot lines
            //GL.Vertex(new Vector3(leftPositionGL.x, leftPositionGL.y, -1.0f));
            //GL.Vertex(new Vector3(rightPositionGL.x, rightPositionGL.y, -1.0f));
            GL.Vertex(leftPositionGL);
            GL.Vertex(rightPositionGL);

            GL.End();
            GL.PopMatrix();

            Vector2 labelPosition = leftPosition;
            labelPosition.x += 5.0f;
            labelPosition.y -= 15.0f;

            Vector2 labelSize = UnityEngine.GUI.skin.GetStyle("ProfilerBadge").CalcSize(new GUIContent(a_level.Label));
            UnityEngine.GUI.Label(new Rect(labelPosition, labelSize), a_level.Label, "ProfilerBadge");

            return true;
        }
    }
}