using System;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Defiant.Debug.GUI
{
    public enum ProfilerMetric
    {
        None,
        FrameTime,
        TotalAllocatedMemory,
        MonoUsedSize,
        MonoHeapSize,

#if UNITY_EDITOR
        Batches,
        DrawCalls,
        SetPassCalls,
        Triangles,
        Vertices,

        RenderTime,
        //ShadowCasters, // NOTE: Doesnt currently work in unity 5.5
        UsedTextureCount,
        TextureMemory,
#endif
    }

    public class ProfilerSamples
    {
        private const int k_maxSamples = 1800; // 60 seconds of samples at 30fps

        private Dictionary<ProfilerMetric, List<float>> m_sampleLists = new Dictionary<ProfilerMetric, List<float>>(new ProfilerMetricComparer());
        

        public void AddSample(ProfilerMetric a_metric, float a_value)
        {
            List<float> list = null;
            if(!m_sampleLists.TryGetValue(a_metric, out list))
            {
                list = new List<float>(k_maxSamples);
                m_sampleLists.Add(a_metric, list);
            }

            if (list.Count == k_maxSamples)
                list.RemoveAt(0);

            list.Add(a_value);
        } 


        public List<float> GetSamples(ProfilerMetric a_metric)
        {
            List<float> list = null;
            m_sampleLists.TryGetValue(a_metric, out list);
            return list;
        }

        public float GetLastSample(ProfilerMetric a_metric)
        {
            List<float> list = null;
            if (!m_sampleLists.TryGetValue(a_metric, out list))
            {
                return 0;
            }

            return list[list.Count - 1];
        }

        /*
        private Dictionary<string, List<float>> m_sampleLists = new Dictionary<string, List<float>>();


        public void AddSample(string a_metric, float a_value)
        {
            List<float> list = null;
            if (!m_sampleLists.TryGetValue(a_metric, out list))
            {
                list = new List<float>(k_maxSamples);
                m_sampleLists.Add(a_metric, list);
            }

            if (list.Count == k_maxSamples)
                list.RemoveAt(0);

            list.Add(a_value);
        }


        public List<float> GetSamples(string a_metric)
        {
            List<float> list = null;
            m_sampleLists.TryGetValue(a_metric, out list);
            return list;
        }

        public float GetLastSample(string a_metric)
        {
            List<float> list = null;
            if (!m_sampleLists.TryGetValue(a_metric, out list))
            {
                return 0;
            }

            return list[list.Count - 1];
        }
        */

        public struct ProfilerMetricComparer : IEqualityComparer<ProfilerMetric>
        {
            public bool Equals(ProfilerMetric x, ProfilerMetric y)
            {
                return x == y;
            }

            public int GetHashCode(ProfilerMetric obj)
            {
                return (int)obj;
            }
        }
    }


    [DebugWindow("Profiler", 1000)]
    public class ProfilerWindow : IDebugWindow
    {
        private const int k_maxSamples = 1800; // 60 seconds of samples at 30fps

        private int m_pixelsPerSample = 8;
        private int m_captureResolution = 1;

        private Vector2 m_scrollPosition = Vector2.zero;

        private ProfilerSamples m_profilerSamples = new ProfilerSamples();

        private ProfilerMetric m_graph0 = ProfilerMetric.FrameTime;
        private ProfilerMetric m_graph1 = ProfilerMetric.MonoUsedSize;
        private ProfilerMetric m_graph2 = ProfilerMetric.None;
        private ProfilerMetric m_graph3 = ProfilerMetric.None;

        private float m_currentFrameTime = 0.0f;
        private float m_minFrameTime = float.MaxValue;
        private float m_maxFrameTime = float.MinValue;

        /*
        private CheatControlBase[] m_controls = new CheatControlBase[]
        {
            new GraphMetricControl(),
            new GraphMetricControl(),
        };
        */

        // TODO: Allow for user supplied metrics. Go back to string key+have event for data supply

        public void Initialize()
        {
        }


        public void Draw(bool a_isSelected)
        {
            // TODO: Maybe add native controls drawing to a window? Add controls and it will handle input on them, then draw as we like? maybe?

            //m_scrollPosition = GUILayout.BeginScrollView(m_scrollPosition);
            {
                GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                GUILayout.Label("Scale:");
                m_pixelsPerSample = (int)GUILayout.HorizontalSlider(m_pixelsPerSample, 20, 1, GUILayout.MaxWidth(150.0f));
                GUILayout.Label("Capture Rate:");
                m_captureResolution = (int)GUILayout.HorizontalSlider(m_captureResolution, 10, 1, GUILayout.MaxWidth(150.0f));
                GUILayout.EndHorizontal();

                DrawInfo();



                //m_controls[0].Draw(false, CheatGUIInputAction.None);
                //m_controls[1].Draw(false, CheatGUIInputAction.None);
                //GUILayout.FlexibleSpace();

                // TODO: Save graph values
                // TODO: Make this work with keyboard and controller
                m_graph0 = DrawPofilerGraph(m_graph0);
                m_graph1 = DrawPofilerGraph(m_graph1);
            }
            //GUILayout.EndScrollView();
        }
        

        private ProfilerMetric DrawPofilerGraph(ProfilerMetric a_metric)
        {
            string graphName = Utils.NicifyVariableName(a_metric.ToString());

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if(GUILayout.Button("<", "ObjectPickerLargeStatus"))
            {
                int metricInt = (int)a_metric;
                --metricInt;
                if(metricInt < 0)
                {
                    metricInt = Enum.GetNames(typeof(ProfilerMetric)).Length - 1;
                }
                a_metric = (ProfilerMetric)metricInt;
            }

            GUILayout.Label(graphName, "ObjectPickerLargeStatus");

            if (GUILayout.Button(">", "ObjectPickerLargeStatus"))
            {
                int metricInt = (int)a_metric;
                ++metricInt;
                if (metricInt > Enum.GetNames(typeof(ProfilerMetric)).Length - 1)
                {
                    metricInt = 0;
                }
                a_metric = (ProfilerMetric)metricInt;
            }
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.Space(-7.0f);

            // TODO: Clean this up
            switch (a_metric)
            {
                case ProfilerMetric.None:
                    break;

                case ProfilerMetric.FrameTime:
                    Graphing.DrawGraph(null, GraphType.Line, m_pixelsPerSample, GetFramerateLevelLabels(), false, m_profilerSamples.GetSamples(a_metric));
                    break;

                case ProfilerMetric.MonoHeapSize:
                case ProfilerMetric.MonoUsedSize:
                case ProfilerMetric.TotalAllocatedMemory:
                    Graphing.DrawGraph(null, GraphType.Line, m_pixelsPerSample, GetMemoryLevelLabels(), true, m_profilerSamples.GetSamples(a_metric));
                    break;

                default:
                    Graphing.DrawGraph(null, GraphType.Line, m_pixelsPerSample, GetGenericLevelLabels(), true, m_profilerSamples.GetSamples(a_metric));
                    break;
            }
            return a_metric;
        }


        private void DrawInfo()
        {
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            Color originalColor = UnityEngine.GUI.color;
            UnityEngine.GUI.color = GetColorForFrameTime(m_currentFrameTime);
            GUILayout.Label("FPS: " + (1.0f/m_currentFrameTime).ToString("0.0"), "CN CountBadge");
            UnityEngine.GUI.color = GetColorForFrameTime(m_minFrameTime);
            GUILayout.Label("Max: " + (1.0f / m_minFrameTime).ToString("0.0"), "CN CountBadge"); // Note: min frame time is max fps, and vice versa
            UnityEngine.GUI.color = GetColorForFrameTime(m_maxFrameTime);
            GUILayout.Label("Min: " + (1.0f / m_maxFrameTime).ToString("0.0"), "CN CountBadge");
            UnityEngine.GUI.color = originalColor;
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.Space(5.0f);
        }


        public bool HandleInput(DebugGUIInputAction a_inputAction)
        {
            return false;
        }


        public void Update(bool a_isSelected)
        {
            if(Time.frameCount % m_captureResolution == 0)
                CollectSample();

            if (a_isSelected)
            {
                m_currentFrameTime = Time.deltaTime;
                List<float> frameTimes = m_profilerSamples.GetSamples(ProfilerMetric.FrameTime);

                if (frameTimes != null)
                {
                    m_maxFrameTime = float.MinValue;
                    m_minFrameTime = float.MaxValue;

                    int framesToTest = Mathf.Min(200, frameTimes.Count);

                    for (int i = frameTimes.Count - 1; i >= frameTimes.Count - framesToTest; --i)
                    {
                        float time = frameTimes[i];

                        if (time > m_maxFrameTime)
                        {
                            m_maxFrameTime = time;
                        }
                        if (time < m_minFrameTime)
                        {
                            m_minFrameTime = time;
                        }
                    }
                }
            }
        }


        public void OnSelected()
        {
        }


        public bool IsAvailable()
        {
            return true;
        }


        private void CollectSample()
        {
            // TODO: Make collections of samples identified via a key and allow for per key settings. This will allow anyone to add metrics externally
            m_profilerSamples.AddSample(ProfilerMetric.FrameTime, Time.deltaTime);
            m_profilerSamples.AddSample(ProfilerMetric.TotalAllocatedMemory, BytesToMBytes(UnityEngine.Profiling.Profiler.GetTotalAllocatedMemory()));
            m_profilerSamples.AddSample(ProfilerMetric.MonoUsedSize, BytesToMBytes(UnityEngine.Profiling.Profiler.GetMonoUsedSize()));
            m_profilerSamples.AddSample(ProfilerMetric.MonoHeapSize, BytesToMBytes(UnityEngine.Profiling.Profiler.GetMonoHeapSize()));

#if UNITY_EDITOR
            m_profilerSamples.AddSample(ProfilerMetric.Batches, UnityStats.batches);
            m_profilerSamples.AddSample(ProfilerMetric.DrawCalls, UnityStats.drawCalls);
            m_profilerSamples.AddSample(ProfilerMetric.SetPassCalls, UnityStats.setPassCalls);
            m_profilerSamples.AddSample(ProfilerMetric.Triangles, UnityStats.triangles);
            m_profilerSamples.AddSample(ProfilerMetric.Vertices, UnityStats.vertices);

            m_profilerSamples.AddSample(ProfilerMetric.RenderTime, UnityStats.renderTime * 1000.0f); // NOTE: render time is in seconds for some reason
            //m_profilerSamples.AddSample(ProfilerMetric.ShadowCasters, UnityStats.shadowCasters); // NOTE: Doesnt currently work in unity 5.5
            m_profilerSamples.AddSample(ProfilerMetric.UsedTextureCount, UnityStats.usedTextureCount);
            m_profilerSamples.AddSample(ProfilerMetric.TextureMemory, BytesToMBytes((uint)UnityStats.usedTextureMemorySize));
#endif
        }


        private float BytesToMBytes(uint a_bytes)
        {
            return a_bytes/1024.0f/1024.0f;
        }


        private float BytesToKBytes(uint a_bytes)
        {
            return a_bytes / 1024.0f;
        }


        GraphLevelLabel[] m_framerateLevels = null;

        public GraphLevelLabel[] GetFramerateLevelLabels()
        {
            if (m_framerateLevels != null)
                return m_framerateLevels;

            Color goodLineColor = new Color(0.8f, 1.0f, 0.8f, 0.5f);
            Color okayLineColor = new Color(1.0f, 1.0f, 0.8f, 0.5f);
            Color badLineColor = new Color(1.0f, 0.8f, 0.8f, 0.5f);

            m_framerateLevels = new GraphLevelLabel[]
            {
                new GraphLevelLabel("1000ms (1FPS)", (1 / 1.0f), badLineColor),
                new GraphLevelLabel("200ms (5FPS)", (1 / 5.0f), badLineColor),
                new GraphLevelLabel("66ms (15FPS)", (1 / 15.0f), badLineColor),
                new GraphLevelLabel("33ms (30FPS)", (1 / 30.0f), okayLineColor),
                new GraphLevelLabel("22ms (45FPS)", (1 / 45.0f), okayLineColor),
                new GraphLevelLabel("16ms (60FPS)", (1 / 60.0f), goodLineColor),
                new GraphLevelLabel("8ms (120FPS)", (1 / 120.0f), goodLineColor),
                new GraphLevelLabel("5ms (200FPS)", (1 / 200.0f), goodLineColor),
                new GraphLevelLabel("1ms (1000FPS)", (1 / 1000.0f), goodLineColor),
                new GraphLevelLabel("0.5ms (2000FPS)", (1 / 2000.0f), goodLineColor),
                new GraphLevelLabel("0ms", 0.0f, goodLineColor),
            };

            return m_framerateLevels;
        }

        GraphLevelLabel[] m_memoryLevels = null;

        public GraphLevelLabel[] GetMemoryLevelLabels()
        {
            if (m_memoryLevels != null)
                return m_memoryLevels;

            m_memoryLevels = new GraphLevelLabel[]
            {
                new GraphLevelLabel("10000mb", 10000.0f),
                new GraphLevelLabel("9000mb", 9000.0f),
                new GraphLevelLabel("8000mb", 8000.0f),
                new GraphLevelLabel("7000mb", 7000.0f),
                new GraphLevelLabel("6000mb", 6000.0f),
                new GraphLevelLabel("5000mb", 5000.0f),
                new GraphLevelLabel("4000mb", 4000.0f),
                new GraphLevelLabel("3000mb", 3000.0f),
                new GraphLevelLabel("2000mb", 2000.0f),
                new GraphLevelLabel("1500mb", 1500.0f),
                new GraphLevelLabel("1250mb", 1250.0f),
                new GraphLevelLabel("1000mb", 1000.0f),
                new GraphLevelLabel("750mb", 750.0f),
                new GraphLevelLabel("500mb", 500.0f),
                new GraphLevelLabel("250mb", 250.0f),
                new GraphLevelLabel("100mb", 100.0f),
                new GraphLevelLabel("75mb", 75.0f),
                new GraphLevelLabel("50mb", 50.0f),
                new GraphLevelLabel("20mb", 20.0f),
                new GraphLevelLabel("10mb", 10.0f),
                new GraphLevelLabel("5mb", 5.0f),
                new GraphLevelLabel("2mb", 2.0f),
                new GraphLevelLabel("1mb", 1.0f),
                new GraphLevelLabel("0mb", 0.0f),
            };

            return m_memoryLevels;
        }


        GraphLevelLabel[] m_genericLevelLabels = null;

        public GraphLevelLabel[] GetGenericLevelLabels()
        {
            if (m_genericLevelLabels != null)
                return m_genericLevelLabels;

            m_genericLevelLabels = new GraphLevelLabel[]
            {
                new GraphLevelLabel("5m", 5000000.0f),
                new GraphLevelLabel("4m", 4000000.0f),
                new GraphLevelLabel("3m", 3000000.0f),
                new GraphLevelLabel("2m", 2000000.0f),
                new GraphLevelLabel("1m", 1000000.0f),
                new GraphLevelLabel("500k", 500000.0f),
                new GraphLevelLabel("200k", 200000.0f),
                new GraphLevelLabel("100k", 100000.0f),
                new GraphLevelLabel("50k", 50000.0f),
                new GraphLevelLabel("30k", 30000.0f),
                new GraphLevelLabel("20k", 20000.0f),
                new GraphLevelLabel("15k", 15000.0f),
                new GraphLevelLabel("10k", 10000.0f),
                new GraphLevelLabel("5k", 5000.0f),
                new GraphLevelLabel("2k", 2000.0f),
                new GraphLevelLabel("1k", 1000.0f),
                new GraphLevelLabel("750", 750.0f),
                new GraphLevelLabel("500", 500.0f),
                new GraphLevelLabel("200", 200.0f),
                new GraphLevelLabel("150", 150.0f),
                new GraphLevelLabel("100", 100.0f),
                new GraphLevelLabel("75", 75.0f),
                new GraphLevelLabel("50", 50.0f),
                new GraphLevelLabel("20", 20.0f),
                new GraphLevelLabel("10", 10.0f),
                new GraphLevelLabel("5", 5.0f),
                new GraphLevelLabel("2", 2.0f),
                new GraphLevelLabel("1", 1.0f),
                new GraphLevelLabel("0.5", 0.5f),
                new GraphLevelLabel("0", 0.0f),
            };

            return m_genericLevelLabels;
        }


        private Color GetColorForFrameTime(float a_time)
        {
            if(a_time < (1/59.0f))
            {
                return new Color(0.5f, 1.0f, 0.5f, 1.0f);
            }
            else if (a_time < (1 / 30.0f))
            {
                return new Color(1.0f, 1.0f, 0.5f, 1.0f);
            }
            else
            {
                return new Color(1.0f, 0.5f, 0.5f, 1.0f);
            }
        }
    }
}
