﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace Defiant.Debug.GUI
{
    public struct SystemInformationEntry
    {
        public readonly string Title;
        public readonly string Value;

        public SystemInformationEntry(string a_title, string a_value)
        {
            Title = a_title;
            Value = a_value;
        }
    }

    public struct SystemInformationGroup
    {
        public readonly string Title;
        public readonly SystemInformationEntry[] Entries;

        public SystemInformationGroup(string a_title, SystemInformationEntry[] a_entries)
        {
            Title = a_title;
            Entries = a_entries;
        }
    }

    [DebugWindow("Info")]
    public class InfoWindow : IDebugWindow
    {
        public SystemInformationGroup[] m_infoGroups = null;
        private Vector2 m_scrollPosition = Vector2.zero;

        public InfoWindow()
        {
        }

        public void Initialize()
        {
        }


        public void Draw(bool a_isSelected)
        {
            if (m_infoGroups == null)
                return;

            m_scrollPosition = GUILayout.BeginScrollView(m_scrollPosition);
            {
                for (int i = 0; i < m_infoGroups.Length; ++i)
                {
                    // TODO: Lay these out nicer
                    DrawInformationGroup(m_infoGroups[i]);
                }
            }
            GUILayout.EndScrollView();
        }


        public bool HandleInput(DebugGUIInputAction a_inputAction)
        {
            if(a_inputAction == DebugGUIInputAction.Down)
            {
                m_scrollPosition.y += 20.0f;
            }

            if (a_inputAction == DebugGUIInputAction.Up)
            {
                m_scrollPosition.y -= 20.0f;
            }

            if(m_scrollPosition.y < 0.0f)
            {
                return false;
            }

            return true;
        }


        public void Update(bool a_isSelected)
        {
        }


        public void OnSelected()
        {
            CollectInformation();
        }


        public bool IsAvailable()
        {
            return true;
        }


        private void DrawInformationGroup(SystemInformationGroup a_group)
        {
            if (a_group.Entries == null)
                return;

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label(a_group.Title, "ObjectPickerLargeStatus");
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            for (int i = 0; i < a_group.Entries.Length; ++i)
            {
                DrawInformationEntry(a_group.Entries[i]);
            }
        }


        private void DrawInformationEntry(SystemInformationEntry a_entry)
        {
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label(a_entry.Title + ':', GUILayout.Width(300.0f));
            //GUILayout.Label(a_entry.Value, GUILayout.ExpandWidth(true));
            GUILayout.Label(a_entry.Value, GUILayout.Width(300.0f));
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
        }


        private void CollectInformation()
        {
            m_infoGroups = new SystemInformationGroup[]
            {
                new SystemInformationGroup("System", new SystemInformationEntry[]
                {
                    new SystemInformationEntry("Operating System", SystemInfo.operatingSystem.ToString()),
                    new SystemInformationEntry("Device Name", SystemInfo.deviceName.ToString()),
                    new SystemInformationEntry("Device Type", SystemInfo.deviceType.ToString()),
                    new SystemInformationEntry("Device Model", SystemInfo.deviceModel),
                    new SystemInformationEntry("CPU Type", SystemInfo.processorType.ToString()),
                    new SystemInformationEntry("CPU Count", SystemInfo.processorCount.ToString()),
                    new SystemInformationEntry("System Memory", Utils.GetBytesReadable(((long)SystemInfo.systemMemorySize) * 1024 * 1024)),
                }),

                new SystemInformationGroup("Unity", new SystemInformationEntry[]
                {
                    new SystemInformationEntry("Version", Application.unityVersion),
                    new SystemInformationEntry("Debug", UnityEngine.Debug.isDebugBuild.ToString()),
                    new SystemInformationEntry("Unity Pro", Application.HasProLicense().ToString()),
                    new SystemInformationEntry("Genuine", Application.genuine ? "Yes" : "No"),
                    new SystemInformationEntry("Genuine Check Available", Application.genuine ? "Yes" : "No"),
                    new SystemInformationEntry("System Language", Application.systemLanguage.ToString()),
                    new SystemInformationEntry("Platform", Application.platform.ToString()),
                    new SystemInformationEntry("IL2CPP", IL2CPP),
                }),

                new SystemInformationGroup("Display", new SystemInformationEntry[]
                {
                    new SystemInformationEntry("Resolution", Screen.width + "x" + Screen.height),
                    new SystemInformationEntry("DPI", Screen.dpi.ToString()),
                    new SystemInformationEntry("Fullscreen", Screen.fullScreen.ToString()),
                    new SystemInformationEntry("Orientation",Screen.orientation.ToString())
                }),

                new SystemInformationGroup("Graphics", new SystemInformationEntry[]
                {
                    new SystemInformationEntry("Device Name", SystemInfo.graphicsDeviceName.ToString()),
                    new SystemInformationEntry("Device Vendor", SystemInfo.graphicsDeviceVendor.ToString()),
                    new SystemInformationEntry("Device Version", SystemInfo.graphicsDeviceVersion.ToString()),
                    new SystemInformationEntry("Device Device ID", SystemInfo.graphicsDeviceID.ToString()),
                    new SystemInformationEntry("Device Vendor ID", SystemInfo.graphicsDeviceVendorID.ToString()),
                    new SystemInformationEntry("Device Memory Size", Utils.GetBytesReadable(((long)SystemInfo.graphicsMemorySize) * 1024 * 1024)),
                    new SystemInformationEntry("Device MultiThreaded", SystemInfo.graphicsMultiThreaded.ToString()),
                    new SystemInformationEntry("Device Shader Level", SystemInfo.graphicsShaderLevel.ToString()),
                    new SystemInformationEntry("Max Tex Size", SystemInfo.maxTextureSize.ToString()),
                    new SystemInformationEntry("NPOT Support", SystemInfo.npotSupport.ToString()),
                    new SystemInformationEntry("3D Textures", SystemInfo.supports3DTextures.ToString()),
                    new SystemInformationEntry("Compute Shaders", SystemInfo.supportsComputeShaders.ToString()),
                    new SystemInformationEntry("Image Effects", SystemInfo.supportsImageEffects.ToString()),
                    new SystemInformationEntry("Instancing", SystemInfo.supportsInstancing.ToString()),
                    new SystemInformationEntry("Motion Vectors", SystemInfo.supportsMotionVectors.ToString()),
                    new SystemInformationEntry("Cubemaps", SystemInfo.supportsRenderToCubemap.ToString()),
                    new SystemInformationEntry("Shadows", SystemInfo.supportsShadows.ToString()),
                    new SystemInformationEntry("Stencil", SystemInfo.supportsStencil.ToString()),
                    new SystemInformationEntry("Sparse Textures", SystemInfo.supportsSparseTextures.ToString())
                }),

                new SystemInformationGroup("Features", new SystemInformationEntry[]
                {
                    new SystemInformationEntry("Accelerometer", SystemInfo.supportsAccelerometer.ToString()),
                    new SystemInformationEntry("Audio", SystemInfo.supportsAudio.ToString()),
                    new SystemInformationEntry("Gyroscope", SystemInfo.supportsGyroscope.ToString()),
                    new SystemInformationEntry("Location Service", SystemInfo.supportsLocationService.ToString()),
                    new SystemInformationEntry("Vibration", SystemInfo.supportsVibration.ToString()),
                }),
            };
        }

#if ENABLE_IL2CPP
        const string IL2CPP = "Yes";
#else
        const string IL2CPP = "No";
#endif
    }
}
