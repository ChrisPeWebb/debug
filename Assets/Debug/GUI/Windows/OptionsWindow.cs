﻿using System.Collections.Generic;

namespace Defiant.Debug.GUI
{
    [DebugWindow("Options", -1000)]
    public class OptionsWindow : BaseDebugWindow
    {
        private DebugCategoryControl m_categoryControl;

        private int m_currentlySelectedCategory = 0;


        public override void Initialize()
        {
            m_categoryControl = new DebugCategoryControl(DebugManager.Instance.Categories);
            m_categoryControl.OnCategoryChanged += OnCategoryChanged;
            Controls.Add(m_categoryControl);

            OnCategoryChanged(0);
        }


        private void OnCategoryChanged(int m_newCategory)
        {
            if (DebugManager.Instance == null || DebugManager.Instance.Categories.Length <= m_newCategory || m_newCategory < 0)
            {
                return;
            }

            CurrentlySelectedControlIndex = 0;
            m_currentlySelectedCategory = m_newCategory;
        }


        private void UpdateControls()
        {
            var activeDebugCategories = DebugManager.Instance.GetCategoriesWithActiveDebugOptions();

            m_categoryControl.SetCategories(DebugManager.Instance.GetCategoriesWithActiveDebugOptions().ToArray());

            Controls.Clear();
            ToolbarControls.Clear();

            // Category control is always first
            ToolbarControls.Add(m_categoryControl);

            if (activeDebugCategories.Count == 0)
                return;

            string category = activeDebugCategories[m_currentlySelectedCategory];

            // Add all methods and values which are part of this category
            List<DebugValue> values = null;
            if (DebugManager.Instance.DebugValues.TryGetValue(category, out values))
            {
                for (int i = 0; i < values.Count; ++i)
                {
                    if (values[i].Type == typeof(bool))
                    {
                        Controls.Add(new DebugBoolValueControl(values[i]));
                    }

                    if (values[i].Type == typeof(int))
                    {
                        Controls.Add(new DebugIntValueControl(values[i]));
                    }

                    if (values[i].Type == typeof(float))
                    {
                        Controls.Add(new DebugFloatValueControl(values[i]));
                    }
                }
            }

            List<DebugMethod> methods = null;
            if (DebugManager.Instance.DebugMethods.TryGetValue(category, out methods))
            {
                for (int i = 0; i < methods.Count; ++i)
                {
                    Controls.Add(new DebugMethodControl(methods[i]));
                }
            }

            List<DynamicDebugMethodSupplier> dynamicMethods = null;
            if (DebugManager.Instance.DynamicDebugMethodSuppliers.TryGetValue(category, out dynamicMethods))
            {
                // Get debug methods supplied by each supplier
                for (int i = 0; i < dynamicMethods.Count; ++i)
                {
                    List<DynamicDebugMethod> suppliedDebugMethods = dynamicMethods[i].Invoke();

                    for (int j = 0; j < suppliedDebugMethods.Count; j++)
                    {
                        Controls.Add(new DynamicDebugMethodControl(suppliedDebugMethods[j]));
                    }
                }
            }
        }


        public override void Update(bool a_isSelected)
        {
            if (a_isSelected)
            {
                UpdateControls();
            }
        }


        public override bool IsAvailable()
        {
            return true;
        }
    }
}
