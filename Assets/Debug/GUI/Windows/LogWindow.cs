﻿using UnityEngine;
using System.Collections.Generic;

namespace Defiant.Debug.GUI
{
    public struct LogEvent
    {
        public readonly string Output;
        public readonly string Stack;
        public readonly LogType Type;

        public LogEvent(string a_output, string a_stack, LogType a_type)
        {
            Output = a_output;
            Stack = a_stack;
            Type = a_type;
        }
    }


    [DebugWindow("Log")]
    public class LogWindow : IDebugWindow
    {
        // TODO: Add filters

        // TODO: Selection still needs to correctly move scroll bar. Its a bit broken at the moment with keyboard

        private Vector2 m_scrollPosition = Vector2.zero;
        private Vector2 m_stackScrollPosition = Vector2.zero;

        private const int k_maxLogs = 500;

        private int m_currentlySelectedLog = 0;

        public List<LogEvent> m_logEvents = new List<LogEvent>(k_maxLogs);


        public LogWindow()
        {
            Application.logMessageReceived += HandleLog;
        }

        public void Initialize()
        {
        }


        public void Draw(bool a_isSelected)
        {
            //float currentY = 0.0f;

            m_scrollPosition = GUILayout.BeginScrollView(m_scrollPosition);
            {
                for (int i = 0; i < m_logEvents.Count; ++i)
                {
                    if(i > 0)
                        GUILayout.Space(-10f);

                    DrawLog(m_logEvents[i], i % 2==0, m_currentlySelectedLog == i && a_isSelected);

                    /*
                    if (i == 0)
                        currentY = -200.0f;

                    currentY += GUILayoutUtility.GetLastRect().height;

                    if(Event.current.type == EventType.Repaint && m_currentlySelectedLog == i && a_isSelected)
                    {
                        m_scrollPosition.y = currentY;
                    }*/

                    Rect guiRect = GUILayoutUtility.GetLastRect();
                    if (Event.current != null && Event.current.isMouse)
                    {
                        if (guiRect.Contains(Event.current.mousePosition))
                        {
                            m_currentlySelectedLog = i;
                        }
                    }
                }
            }

            GUILayout.FlexibleSpace();
            GUILayout.EndScrollView();

            DrawStack();
        }


        public bool HandleInput(DebugGUIInputAction a_inputAction)
        {
            if(a_inputAction == DebugGUIInputAction.Down)
            {
                ++m_currentlySelectedLog;

                if(m_currentlySelectedLog > m_logEvents.Count-1)
                {
                    m_currentlySelectedLog = m_logEvents.Count - 1;
                    return false;
                }
            }

            if (a_inputAction == DebugGUIInputAction.Up)
            {
                --m_currentlySelectedLog;

                if (m_currentlySelectedLog < 0)
                {
                    m_currentlySelectedLog = m_logEvents.Count - 1;
                    return false;
                }
            }

            if (m_logEvents.Count == 0)
            {
                return false;
            }


            return true;
        }


        public void Update(bool a_isSelected)
        {

        }


        public void OnSelected()
        {
            m_currentlySelectedLog = m_logEvents.Count - 1;
        }


        public bool IsAvailable()
        {
            return true;
        }


        // TODO: For consistencies sake, should log entries be drawn as controls?
        private void DrawLog(LogEvent a_log, bool a_isEven, bool a_isSelected)
        {
            if (a_isEven)
            {
                //GUILayout.BeginHorizontal("CN EntryBackEven", GUILayout.MaxHeight(30.0f));
            }
            else
            {
                //GUILayout.BeginHorizontal("CN EntryBackOdd", GUILayout.MaxHeight(30.0f));
            }

            Color originalColor = UnityEngine.GUI.backgroundColor;
            if(!a_isSelected)
            {
                UnityEngine.GUI.backgroundColor = Color.clear;
            }
            GUILayout.BeginVertical("U2D.createRect", GUILayout.MaxHeight(30.0f));
            UnityEngine.GUI.backgroundColor = originalColor;

            switch (a_log.Type)
            {
                case LogType.Log:
                    GUILayout.BeginHorizontal("CN EntryInfo");
                    break;
                case LogType.Warning:
                    GUILayout.BeginHorizontal("CN EntryWarn");
                    break;
                case LogType.Error:
                case LogType.Assert:
                case LogType.Exception:
                    GUILayout.BeginHorizontal("CN EntryError");
                    break;
            }

            GUILayout.Label(a_log.Output, GUILayout.MaxHeight(30.0f));

            GUILayout.EndHorizontal();

            GUILayout.EndVertical();

            //GUILayout.EndHorizontal();
        }


        private void DrawStack()
        {
            GUILayout.BeginVertical("Box", GUILayout.Height(100.0f));

            m_stackScrollPosition = GUILayout.BeginScrollView(m_stackScrollPosition);
            {
                if (m_logEvents.Count > 0)
                {
                    if(m_currentlySelectedLog > m_logEvents.Count-1)
                    {
                        m_currentlySelectedLog = m_logEvents.Count - 1;
                    }

                    GUILayout.Label(m_logEvents[m_currentlySelectedLog].Output);
                    GUILayout.Label(m_logEvents[m_currentlySelectedLog].Stack);
                }
                else
                {
                }
            }
            GUILayout.EndScrollView();

            GUILayout.EndVertical();
        }


        private void HandleLog(string a_output, string a_stack, LogType a_type)
        {
            if (m_logEvents.Count == k_maxLogs)
            {
                --m_currentlySelectedLog;

                if (m_currentlySelectedLog < 0)
                {
                    m_currentlySelectedLog = 0;
                }

                m_logEvents.RemoveAt(0);
            }

            if (m_currentlySelectedLog == m_logEvents.Count - 1)
            {
                ++m_currentlySelectedLog;
                m_scrollPosition.y = Mathf.Infinity;
            }

            m_logEvents.Add(new LogEvent(a_output, a_stack, a_type));

            // If we were selecting the last log, automatically bump the scrollview down
            if (m_currentlySelectedLog == m_logEvents.Count - 1)
            {
                m_scrollPosition.y = Mathf.Infinity;
            }

        }
    }
}
