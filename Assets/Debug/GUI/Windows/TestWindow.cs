﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace Defiant.Debug.GUI
{
    public class TestControl : BaseDebugControl
    {
        float m_height;
        bool m_selectable;

        public TestControl(float a_height = 30.0f, bool a_selectable = true)
        {
            m_height = a_height;
            m_selectable = a_selectable;
        }

        protected override void DrawInternal()
        {
            if(m_selectable)
            {
                GUILayout.Label("Selectable", GUILayout.Height(m_height));
            }
            else
            {
                GUILayout.Label("Not Selectable", GUILayout.Height(m_height));
            }
            //GUILayout.Label("Test Control", GUILayout.Height(m_height));
        }

        protected override void HandleInputInternal(DebugGUIInputAction a_inputAction)
        {

        }

        public override bool Selectable()
        {
            return m_selectable;
        }
    }

    //[DebugWindow("Test Window", -2000)]
    public class TestWindow : BaseDebugWindow
    {
        public override void Initialize()
        {
            Controls.Add(new TestControl());
            Controls.Add(new TestControl(60, false));
            Controls.Add(new TestControl(60));
            Controls.Add(new TestControl());
            Controls.Add(new TestControl(120));
            Controls.Add(new TestControl(60));
            Controls.Add(new TestControl());
            Controls.Add(new TestControl(60, false));
            Controls.Add(new TestControl());
            Controls.Add(new TestControl(60, false));
            Controls.Add(new TestControl(120));
            Controls.Add(new TestControl());
            Controls.Add(new TestControl(60));
            Controls.Add(new TestControl());
            Controls.Add(new TestControl(120));
            Controls.Add(new TestControl(60, false));
            Controls.Add(new TestControl());
            Controls.Add(new TestControl(60, false));
            Controls.Add(new TestControl(60, false));
            Controls.Add(new TestControl(60));
            Controls.Add(new TestControl());
            Controls.Add(new TestControl(120));
            Controls.Add(new TestControl());
            Controls.Add(new TestControl(60));
            Controls.Add(new TestControl());
            Controls.Add(new TestControl(60, false));
            Controls.Add(new TestControl(120));
            Controls.Add(new TestControl(60));
            Controls.Add(new TestControl());
            Controls.Add(new TestControl(60, false));
            Controls.Add(new TestControl(60));
            Controls.Add(new TestControl());
            Controls.Add(new TestControl(120));
        }

        public override bool IsAvailable()
        {
            return true;
        }

        public override void OnSelected()
        {
        }

        public override void Update(bool a_isSelected)
        {
        }
    }
}
