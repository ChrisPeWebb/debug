﻿using UnityEngine;

namespace Defiant.Debug.GUI
{
    public interface IDebugWindow
    {
        void Initialize();

        /// <summary>
        /// Draws the window
        /// </summary>
        void Draw(bool a_isSelected);

        void Update(bool a_isSelected);

        void OnSelected();


        /// <summary>
        /// Gives the latest input to the window.
        /// </summary>
        /// <param name="a_inputAction"></param>
        /// <returns>Returns true when the window still wants to be selected. Returns false when the window is relinquishing selection</returns>
        bool HandleInput(DebugGUIInputAction a_inputAction);

        bool IsAvailable();
    }
}
