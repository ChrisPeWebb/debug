﻿using System.Reflection;
using System.Collections.Generic;


namespace Defiant.Debug
{
    public class DynamicDebugMethodSupplier
    {
        private MethodInfo m_method;


        public DynamicDebugMethodSupplier(MethodInfo a_method)
        {
            m_method = a_method;
        }


        public List<DynamicDebugMethod> Invoke()
        {
            return (List<DynamicDebugMethod>)m_method.Invoke(null, null);
        }
    }
}