﻿namespace Defiant.Debug
{
    public class DynamicDebugMethod
    {
        public System.Action Action { get; private set; }
        public string Name { get; private set; }
        public string Description { get; private set; }


        public DynamicDebugMethod(System.Action a_action, string a_name, string a_description = "")
        {
            Action = a_action;
            Name = a_name;
            Description = a_description;
        }
    }
}
