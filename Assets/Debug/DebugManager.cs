﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using Defiant.Debug.GUI;

namespace Defiant.Debug
{
    public class DebugManager
    {
        private const string k_defaultCategoryName = "No Category";

        private static DebugManager m_instance;
        public static DebugManager Instance
        {
            get
            {
                // Debug are only available in editor, if DEBUG_MENU and when built in development mode
#if !DEBUG_MENU
                if (!UnityEngine.Debug.isDebugBuild)
                {
                    return null;
                }
#endif 
                if (m_instance != null)
                {
                    return m_instance;
                }

                m_instance = new DebugManager();
                return m_instance;
            }
        }

        public string[] Categories { get; private set; }
        
        // Debug windows sorted in ascending order by attribute Order property
        public List<KeyValuePair<DebugWindowAttribute,IDebugWindow>> DebugWindows { get; private set; }

        // Debug methods sorted by category. Methods with no category have the key k_defaultCategoryName
        public Dictionary<string, List<DebugMethod>> DebugMethods { get; private set; }

        // Debug values sorted by category. Value with no category have the key k_defaultCategoryName
        public Dictionary<string, List<DebugValue>> DebugValues { get; private set; }

        // Dynamic debug method suppliers sorted by category. Methods with no category have the key k_defaultCategoryName
        public Dictionary<string, List<DynamicDebugMethodSupplier>> DynamicDebugMethodSuppliers { get; private set; }

        private HashSet<string> m_categoriesSet = new HashSet<string>();

        private Type[] m_acceptedTypes =
        {
            typeof(bool),
            typeof(int),
            typeof(float)
        };

        // TODO: Allow class instances to register with manager. Scan them and pull their debug options out for display


        public DebugManager()
        {
            Initialize();
        }


        private void Initialize()
        {
            DebugWindows = new List<KeyValuePair<DebugWindowAttribute, IDebugWindow>>(3);
            DebugMethods = new Dictionary<string, List<DebugMethod>>(10);
            DebugValues = new Dictionary<string, List<DebugValue>>(10);
            DynamicDebugMethodSuppliers = new Dictionary<string, List<DynamicDebugMethodSupplier>>(5);

            FindDebugMethods();
            FindDynamicDebugMethods();
            FindDebugFields();
            FindDebugProperties();
            FindCategories();

            FindDebugWindows();
        }


        private void FindDebugWindows()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            Type[] debugWindows = assembly.GetTypes()
                      .Where(m => m.GetCustomAttributes(typeof(DebugWindowAttribute), false).Length > 0)
                      .ToArray();

            for (int i = 0; i < debugWindows.Length; ++i)
            {
                // TODO: Validate these

                DebugWindowAttribute debugWindowAttribute = GetAttribute<DebugWindowAttribute>(debugWindows[i]);

                IDebugWindow instance = (IDebugWindow)Activator.CreateInstance(debugWindows[i]);
                DebugWindows.Add(new KeyValuePair<DebugWindowAttribute, IDebugWindow>(debugWindowAttribute, instance));
            }

            // order windows by order
            DebugWindows.Sort((x, y) => x.Key.Order.CompareTo(y.Key.Order));
        }


        private void FindDebugMethods()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            MethodInfo[] debugMethods = assembly.GetTypes()
                      .SelectMany(t => t.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static))
                      .Where(m => m.GetCustomAttributes(typeof(DebugAttribute), false).Length > 0)
                      .ToArray();

            // Sort debug methods and insert into dictionary
            for(int i = 0; i < debugMethods.Length; ++i)
            {
                if (!ValidateMethod(debugMethods[i]))
                {
                    continue;
                }

                DebugAttribute debugAttribute = GetAttribute<DebugAttribute>(debugMethods[i]);
                DebugCategoryAttribute categoryAttribute = GetAttribute<DebugCategoryAttribute>(debugMethods[i]);
                string name = string.IsNullOrEmpty(debugAttribute.Name) ? debugMethods[i].Name : debugAttribute.Name;
                DebugMethod debugMethod = new DebugMethod(name, debugAttribute.Description, debugMethods[i]);

                if (categoryAttribute != null)
                {
                    List<DebugMethod> methodList = null;
                    if(DebugMethods.TryGetValue(categoryAttribute.Category, out methodList))
                    {
                        methodList.Add(debugMethod);
                    }
                    else
                    {
                        methodList = new List<DebugMethod>();
                        methodList.Add(debugMethod);
                        DebugMethods.Add(categoryAttribute.Category, methodList);
                    }
                }
                else
                {
                    List<DebugMethod> methodList = null;
                    if (DebugMethods.TryGetValue(k_defaultCategoryName, out methodList))
                    {
                        methodList.Add(debugMethod);
                    }
                    else
                    {
                        methodList = new List<DebugMethod>();
                        methodList.Add(debugMethod);
                        DebugMethods.Add(k_defaultCategoryName, methodList);
                    }
                }
            }
        }


        private void FindDynamicDebugMethods()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            MethodInfo[] debugMethods = assembly.GetTypes()
                      .SelectMany(t => t.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static))
                      .Where(m => m.GetCustomAttributes(typeof(DynamicDebugMethodSupplierAttribute), false).Length > 0)
                      .ToArray();

            // Sort debug methods and insert into dictionary
            for (int i = 0; i < debugMethods.Length; ++i)
            {
                if (!ValidateDynamicMethod(debugMethods[i]))
                {
                    continue;
                }

                DynamicDebugMethodSupplierAttribute debugSupplierAttribute = GetAttribute<DynamicDebugMethodSupplierAttribute>(debugMethods[i]);
                DynamicDebugMethodSupplier dynamicDebugMethodSupplier = new DynamicDebugMethodSupplier(debugMethods[i]);

                
                if (debugSupplierAttribute.Category != null)
                {
                    List<DynamicDebugMethodSupplier> methodList = null;
                    if (DynamicDebugMethodSuppliers.TryGetValue(debugSupplierAttribute.Category, out methodList))
                    {
                        methodList.Add(dynamicDebugMethodSupplier);
                    }
                    else
                    {
                        methodList = new List<DynamicDebugMethodSupplier>();
                        methodList.Add(dynamicDebugMethodSupplier);
                        DynamicDebugMethodSuppliers.Add(debugSupplierAttribute.Category, methodList);
                    }
                }
                else
                {
                    List<DynamicDebugMethodSupplier> methodList = null;
                    if (DynamicDebugMethodSuppliers.TryGetValue(k_defaultCategoryName, out methodList))
                    {
                        methodList.Add(dynamicDebugMethodSupplier);
                    }
                    else
                    {
                        methodList = new List<DynamicDebugMethodSupplier>();
                        methodList.Add(dynamicDebugMethodSupplier);
                        DynamicDebugMethodSuppliers.Add(k_defaultCategoryName, methodList);
                    }
                }
            }
        }


        private void FindDebugFields()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            FieldInfo[] debugFields = assembly.GetTypes()
                      .SelectMany(t => t.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static))
                      .Where(m => m.GetCustomAttributes(typeof(DebugAttribute), false).Length > 0)
                      .ToArray();

            // Sort debug fields and insert into dictionary
            for (int i = 0; i < debugFields.Length; ++i)
            {
                if (!ValidateField(debugFields[i]))
                {
                    continue;
                }

                DebugAttribute debugAttribute = GetAttribute<DebugAttribute>(debugFields[i]);
                DebugCategoryAttribute categoryAttribute = GetAttribute<DebugCategoryAttribute>(debugFields[i]);
                string nameame = string.IsNullOrEmpty(debugAttribute.Name) ? debugFields[i].Name : debugAttribute.Name;
                DebugValue debugValue = new DebugValue(nameame, debugAttribute.Description, debugFields[i]);

                string category = (categoryAttribute != null) ? categoryAttribute.Category : k_defaultCategoryName;

                List<DebugValue> valueList = null;
                if (DebugValues.TryGetValue(category, out valueList))
                {
                    valueList.Add(debugValue);
                }
                else
                {
                    valueList = new List<DebugValue>();
                    valueList.Add(debugValue);
                    DebugValues.Add(category, valueList);
                }
            }
        }
        

        private void FindDebugProperties()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            PropertyInfo[] debugProperties = assembly.GetTypes()
                      .SelectMany(t => t.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static))
                      .Where(m => m.GetCustomAttributes(typeof(DebugAttribute), false).Length > 0)
                      .ToArray();

            // Sort debug properties and insert into dictionary
            for (int i = 0; i < debugProperties.Length; ++i)
            {
                if(!ValidateProperty(debugProperties[i]))
                {
                    continue;
                }

                DebugAttribute debugAttribute = GetAttribute<DebugAttribute>(debugProperties[i]);
                DebugCategoryAttribute categoryAttribute = GetAttribute<DebugCategoryAttribute>(debugProperties[i]);
                string name = string.IsNullOrEmpty(debugAttribute.Name) ? debugProperties[i].Name : debugAttribute.Name;
                DebugValue debugValue = new DebugValue(name, debugAttribute.Description, debugProperties[i]);

                string category = (categoryAttribute != null) ? categoryAttribute.Category : k_defaultCategoryName;

                List<DebugValue> valueList = null;
                if (DebugValues.TryGetValue(category, out valueList))
                {
                    valueList.Add(debugValue);
                }
                else
                {
                    valueList = new List<DebugValue>();
                    valueList.Add(debugValue);
                    DebugValues.Add(category, valueList);
                }
            }
        }


        private void FindCategories()
        {
            m_categoriesSet.Clear();

            foreach(string category in DebugMethods.Keys)
            {
                m_categoriesSet.Add(category);
            }

            foreach (string category in DebugValues.Keys)
            {
                m_categoriesSet.Add(category);
            }

            foreach (string category in DynamicDebugMethodSuppliers.Keys)
            {
                m_categoriesSet.Add(category);
            }

            Categories = m_categoriesSet.ToArray<string>();

            // Sort categories
            Array.Sort(Categories, StringComparer.InvariantCulture);
        }


        public static T GetAttribute<T>(MethodInfo a_methodInfo) where T : System.Attribute
        {
            object[] customAttributes = a_methodInfo.GetCustomAttributes(false);

            for(int i = 0; i < customAttributes.Length; ++i)
            {
                T desiredAttribute = customAttributes[i] as T;
                if(desiredAttribute != null)
                {
                    return desiredAttribute;
                }
            }

            return null;
        }

        public static T GetAttribute<T>(FieldInfo a_fieldInfo) where T : System.Attribute
        {
            object[] customAttributes = a_fieldInfo.GetCustomAttributes(false);

            for (int i = 0; i < customAttributes.Length; ++i)
            {
                T desiredAttribute = customAttributes[i] as T;
                if (desiredAttribute != null)
                {
                    return desiredAttribute;
                }
            }

            return null;
        }

        public static T GetAttribute<T>(PropertyInfo a_propertyInfo) where T : System.Attribute
        {
            object[] customAttributes = a_propertyInfo.GetCustomAttributes(false);

            for (int i = 0; i < customAttributes.Length; ++i)
            {
                T desiredAttribute = customAttributes[i] as T;
                if (desiredAttribute != null)
                {
                    return desiredAttribute;
                }
            }

            return null;
        }

        public static T GetAttribute<T>(Type a_type) where T : System.Attribute
        {
            object[] customAttributes = a_type.GetCustomAttributes(false);

            for (int i = 0; i < customAttributes.Length; ++i)
            {
                T desiredAttribute = customAttributes[i] as T;
                if (desiredAttribute != null)
                {
                    return desiredAttribute;
                }
            }

            return null;
        }


        public List<string> GetCategoriesWithActiveDebugOptions()
        {
            List<String> categories = new List<string>();

            for(int i = 0; i < Categories.Length; ++i)
            {
                if(DoesCategoryHaveValidDebugOptions(Categories[i]))
                {
                    categories.Add(Categories[i]);
                }
            }

            return categories;
        } 


        public bool DoesCategoryHaveValidDebugOptions(string a_category)
        {
            List<DebugValue> debugValues = null;
            if(DebugValues.TryGetValue(a_category, out debugValues))
            {
                if (debugValues.Count > 0)
                    return true;
            }

            List<DebugMethod> debugMethods = null;
            if (DebugMethods.TryGetValue(a_category, out debugMethods))
            {
                if (debugMethods.Count > 0)
                    return true;
            }


            List<DynamicDebugMethodSupplier> debugMethodSuppliers = null;
            if (DynamicDebugMethodSuppliers.TryGetValue(a_category, out debugMethodSuppliers))
            {
                for(int i = 0; i < debugMethodSuppliers.Count; ++i)
                {
                    var dynamicDebugMethods = debugMethodSuppliers[i].Invoke();

                    if(dynamicDebugMethods != null && dynamicDebugMethods.Count > 0)
                    {
                        return true;
                    }
                }
            }

            return false;
        }


        private bool ValidateMethod(MethodInfo a_methodInfo)
        {
            // Debug must be static
            if(!a_methodInfo.IsStatic)
            {
                UnityEngine.Debug.LogError("Debug method must be static: " + a_methodInfo.DeclaringType.Name + "." + a_methodInfo.Name);
                return false;
            }

            // Debug must require no parameters (Currently)
            if(a_methodInfo.GetParameters().Length > 0)
            {
                UnityEngine.Debug.LogError("Debug method must not require parameters: " + a_methodInfo.DeclaringType.Name + "." + a_methodInfo.Name);
                return false;
            }

            return true;
        }


        private bool ValidateDynamicMethod(MethodInfo a_methodInfo)
        {
            // Debug must be static
            if (!a_methodInfo.IsStatic)
            {
                UnityEngine.Debug.LogError("Dynamic debug method must be static: " + a_methodInfo.DeclaringType.Name + "." + a_methodInfo.Name);
                return false;
            }

            // Debug must require no parameters (Currently)
            if (a_methodInfo.GetParameters().Length > 0)
            {
                UnityEngine.Debug.LogError("Dynamic debug method must not require parameters: " + a_methodInfo.DeclaringType.Name + "." + a_methodInfo.Name);
                return false;
            }

            // Dynamic debug method must return a list of DynamicDebugMethods
            if (a_methodInfo.ReturnType != typeof(List<DynamicDebugMethod>))
            {
                UnityEngine.Debug.LogError("Dynamic debug method must return type List<DynamicDebugMethod>: " + a_methodInfo.DeclaringType.Name + "." + a_methodInfo.Name);
                return false;
            }

            return true;
        }


        private bool ValidateField(FieldInfo a_fieldInfo)
        {
            // Debug must be static
            if (!a_fieldInfo.IsStatic)
            {
                UnityEngine.Debug.LogError("Debug field must be static: " + a_fieldInfo.DeclaringType.Name + "." + a_fieldInfo.Name);
                return false;
            }

            Type fieldType = a_fieldInfo.FieldType;

            // Check debug for typing (not the best way. Checking against actual drawers would work better)
            bool typeFound = false;
            for(int i = 0; i < m_acceptedTypes.Length; ++i)
            {
                if(fieldType == m_acceptedTypes[i])
                {
                    typeFound = true;
                    break;
                }
            }

            if (!typeFound)
            {
                string acceptedTypes = "";
                for (int i = 0; i < m_acceptedTypes.Length; ++i)
                {
                    acceptedTypes += m_acceptedTypes[i];
                    if(i < m_acceptedTypes.Length-1)
                    {
                        acceptedTypes += ", ";
                    }
                }

                UnityEngine.Debug.LogError("Debug field must be a of type " + acceptedTypes + ": " + a_fieldInfo.DeclaringType.Name + "." + a_fieldInfo.Name);
                return false;
            }

            return true;
        }


        private bool ValidateProperty(PropertyInfo a_propertyInfo)
        {
            // Ensure property can be read statically
            //if(!a_propertyInfo.CanRead || a_propertyInfo.GetGetMethod() == null || !a_propertyInfo.GetGetMethod().IsStatic)
            if (!a_propertyInfo.CanRead)
            {
                UnityEngine.Debug.LogError("Debug property must be readable statically: " + a_propertyInfo.DeclaringType.Name + "." + a_propertyInfo.Name);
                return false;
            }

            // Ensure property can be written to statically
            //if (!a_propertyInfo.CanWrite || a_propertyInfo.GetSetMethod() == null || !a_propertyInfo.GetSetMethod().IsStatic)
            if (!a_propertyInfo.CanWrite)
            {
                UnityEngine.Debug.LogError("Debug property must be writable statically: " + a_propertyInfo.DeclaringType.Name + "." + a_propertyInfo.Name);
                return false;
            }

            
            Type propertyType = a_propertyInfo.PropertyType;

            // Check debug for typing (not the best way. Checking against actual drawers would work better)
            bool typeFound = false;
            for (int i = 0; i < m_acceptedTypes.Length; ++i)
            {
                if (propertyType == m_acceptedTypes[i])
                {
                    typeFound = true;
                    break;
                }
            }

            if (!typeFound)
            {
                string acceptedTypes = "";
                for (int i = 0; i < m_acceptedTypes.Length; ++i)
                {
                    acceptedTypes += m_acceptedTypes[i];
                    if (i < m_acceptedTypes.Length - 1)
                    {
                        acceptedTypes += ", ";
                    }
                }

                UnityEngine.Debug.LogError("Debug property must be a of type " + acceptedTypes + ": " + a_propertyInfo.DeclaringType.Name + "." + a_propertyInfo.Name);
                return false;
            }

            return true;
        }
    }
}

