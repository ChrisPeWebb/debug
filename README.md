![Cheats.PNG](https://bitbucket.org/repo/xpe49j/images/3845429266-Cheats.PNG)


### What does it do? ###
Easily exposes static fields, properties and methods to a GUI which supports mouse, keyboard and controller input.  
Currently only supports static fields and properties of bool type, and static parameter-less methods.  
Cheat states are saved and loaded from player prefs.



### How do I use it? ###
* Create a new gameobject and attach the CheatGUI component to it.   
* If you would like keyboard support, attach the CheatGUIKeyboardInput component.  
* If you would like Controller support, attach the CheatGUIControllerInput component, and modify the script to work with whatever control system you use.  
* Add the CheatAttribute to a static field, property or method. You can optionally name the cheat and add a description.  
* Optionally, you can also add the CheatCategoryAttribute also. Cheats will be sorted by category.  
* View TestClass.cs for an example.